package de.tenyrian.skill;

import de.tenyrian.Tenyrian;
import de.tenyrian.config.EConfig;
import de.tenyrian.rpgplayer.RpgPlayer;

public abstract class Skill {

	public Tenyrian plugin;
	public EConfig eConfig;
	
	public String name = "DefaultName";
	public String description = "DefaultDescription";
	public int level = 0;
	public int cooldown = 0;
	public int mana = 0;
	public int damage = 0;
	
	public void load(){
		
		plugin = Tenyrian.getInstance();
		eConfig = new EConfig(plugin, "skill/" + name + ".yml");
		if(!eConfig.getYml().contains("skill")){
			
			eConfig.getYml().set("skill.level", level);
			eConfig.getYml().set("skill.cooldown", cooldown);
			eConfig.getYml().set("skill.mana", mana);
			eConfig.getYml().set("skill.damage", damage);
			eConfig.save();
		}else{
			
			level = eConfig.getYml().getInt("skill.level", level);
			cooldown = eConfig.getYml().getInt("skill.cooldown", cooldown);
			mana = eConfig.getYml().getInt("skill.mana", mana);
			damage = eConfig.getYml().getInt("skill.damage", damage);
		}
		
	}
	
	public abstract void cast(RpgPlayer rpgPlayer);

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescripton() {
		return description;
	}

	public void setDescripton(String descripton) {
		this.description = descripton;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getCooldown() {
		return cooldown;
	}

	public void setCooldown(int cooldown) {
		this.cooldown = cooldown;
	}

	public int getMana() {
		return mana;
	}

	public void setMana(int mana) {
		this.mana = mana;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}
}
