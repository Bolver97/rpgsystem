package de.tenyrian.skill.barbar;

import de.tenyrian.rpgplayer.RpgPlayer;
import de.tenyrian.skill.Skill;

public class SkillHieb extends Skill{

	public SkillHieb() {

		setName("Hieb");
		setDescripton("Der Barbar führt einen Hieb aus, der dem Gegner Schaden zufügt.");
		setLevel(5);
		setMana(5);
		setDamage(110);
		setCooldown(4);
	}
	
	@Override
	public void cast(RpgPlayer rpgPlayer) {

		
	}
}
