package de.tenyrian.command.player;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.avaje.ebean.validation.Range;

import de.tenyrian.Tenyrian;
import de.tenyrian.chat.ChatType;
import de.tenyrian.chat.CommandListener;
import de.tenyrian.klasse.Klassen;
import de.tenyrian.language.Chat;
import de.tenyrian.language.LanguageManager;
import de.tenyrian.rank.Rank;
import de.tenyrian.rpgplayer.RpgPlayer;
import de.tenyrian.utils.MathUtils;
import de.tenyrian.utils.PlayerUtils;
import de.tenyrian.utils.TenyrianUtils;

public class CommandChat extends CommandListener{

	private Tenyrian plugin;
	
	public CommandChat(Tenyrian plugin) {
		
		this.plugin = plugin;
		
		LanguageManager lm = plugin.getLanguageManager();
	
		lm.addDefaultString("chat.player.argument", "Der Chat @a existiert nicht!");
		lm.addDefaultString("chat.player.success.sender", "Du bist in den Chat @a gewechselt!");
	}

	@Override
	public boolean onCommand(Player player, String cmd, String[] args) {
		
		String prefix = "Info";
		String command = "/chat <local/global/guild>";
		
		if(args.length != 1){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.CommandUsage, new String[]{"@cmd=" + command});
			return true;
		}
		
		switch (args[0]) {
		case "global":
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, "chat.player.success.sender", new String[]{"@a=global"});
			PlayerUtils.setChat(player, ChatType.Global);
			return true;
			
		case "local":
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, "chat.player.success.sender", new String[]{"@a=local"});
			PlayerUtils.setChat(player, ChatType.Local);
			return true;
		case "guild":
			
			//Todo
			return false;			
		default:
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, "chat.player.argument", new String[]{"@a=" + args[0]});
			return true;
		}
	}
	
	public void showInfo(Player sender, Player player){
	
		RpgPlayer rpgPlayer = PlayerUtils.getRpgPlayer(player);
		String language = PlayerUtils.getRpgPlayer(sender).getLanguage();
		int klasse = rpgPlayer.getKlass();
		
		Chat.sendMenue(sender, "Info", 7);
		Chat.sendMessage(sender, Chat.DEFAULT, "", "info.player.rang", new String[]{"@a=" + Rank.getRankName(rpgPlayer.getRank())});
		Chat.sendMessage(sender, Chat.DEFAULT, "", "info.player.klasse", new String[]{"@a=" + Klassen.getKlassenName(language, klasse)});
		Chat.sendMessage(sender, Chat.DEFAULT, "", "info.player.level", new String[]{"@a=" + rpgPlayer.getLevel()});
		Chat.sendMessage(sender, Chat.DEFAULT, "", "info.player.xp", new String[]{"@a01=" + rpgPlayer.getXP(), "@a02=" + Klassen.getMaxXP(rpgPlayer.getLevel())});
		Chat.sendMessage(sender, Chat.DEFAULT, "", "info.player.gilde", new String[]{"@a=-"});
	}
}
