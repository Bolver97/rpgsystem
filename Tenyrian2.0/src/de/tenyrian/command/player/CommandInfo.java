package de.tenyrian.command.player;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.avaje.ebean.validation.Range;

import de.tenyrian.Tenyrian;
import de.tenyrian.chat.CommandListener;
import de.tenyrian.klasse.Klassen;
import de.tenyrian.language.Chat;
import de.tenyrian.language.LanguageManager;
import de.tenyrian.rank.Rank;
import de.tenyrian.rpgplayer.RpgPlayer;
import de.tenyrian.utils.PlayerUtils;

public class CommandInfo extends CommandListener{

	private Tenyrian plugin;
	
	public CommandInfo(Tenyrian plugin) {
		
		this.plugin = plugin;
		
		LanguageManager lm = plugin.getLanguageManager();
		
		lm.addDefaultString("info.player.offline", "Der Spieler @p ist momentan nicht Online!");
		lm.addDefaultString("info.player.rang", "Rang: @a");
		lm.addDefaultString("info.player.klasse", "Klasse: @a");
		lm.addDefaultString("info.player.level", "Level: @a");
		lm.addDefaultString("info.player.xp", "XP: @a01/@a02");
		lm.addDefaultString("info.player.gilde", "Gilde: @a");
	}

	@Override
	public boolean onCommand(Player player, String cmd, String[] args) {
		
		String prefix = "Info";
		String command = "/info <player>";
		
		if(args.length != 1 && args.length != 0){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.CommandUsage, new String[]{"@cmd=" + command});
			return true;
		}
		
		if(args.length == 1){
			
			Player arg = Bukkit.getPlayer(args[0]);
			if(arg == null){
				
				Chat.sendMessage(player, Chat.PREFIX, prefix, "info.player.offline", new String[]{"@p=" + args[0]});
				return true;
			}
			
			showInfo(player, arg);
		}
		
		if(args.length == 0){
			
			showInfo(player, player);
		}
		
		return true;
	}
	
	public void showInfo(Player sender, Player player){
	
		RpgPlayer rpgPlayer = PlayerUtils.getRpgPlayer(player);
		String language = PlayerUtils.getRpgPlayer(sender).getLanguage();
		int klasse = rpgPlayer.getKlass();
		
		Chat.sendMenue(sender, player.getName() + "'s Info", 7);
		Chat.sendMessage(sender, Chat.DEFAULT, "", "info.player.rang", new String[]{"@a=" + Rank.getRankName(rpgPlayer.getRank())});
		Chat.sendMessage(sender, Chat.DEFAULT, "", "info.player.klasse", new String[]{"@a=" + Klassen.getKlassenName(language, klasse)});
		Chat.sendMessage(sender, Chat.DEFAULT, "", "info.player.level", new String[]{"@a=" + rpgPlayer.getLevel()});
		Chat.sendMessage(sender, Chat.DEFAULT, "", "info.player.xp", new String[]{"@a01=" + rpgPlayer.getXP(), "@a02=" + Klassen.getMaxXP(rpgPlayer.getLevel())});
		Chat.sendMessage(sender, Chat.DEFAULT, "", "info.player.gilde", new String[]{"@a=-"});
	}
}
