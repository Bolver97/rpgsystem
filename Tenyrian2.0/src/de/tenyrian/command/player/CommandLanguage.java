package de.tenyrian.command.player;

import org.bukkit.entity.Player;

import de.tenyrian.Tenyrian;
import de.tenyrian.chat.CommandListener;
import de.tenyrian.language.Chat;
import de.tenyrian.language.LanguageManager;
import de.tenyrian.menue.MenueManager;
import de.tenyrian.menue.player.MenueSetLanguage;
import de.tenyrian.utils.TenyrianUtils;

public class CommandLanguage extends CommandListener{

	private Tenyrian plugin;
	private MenueSetLanguage menueSetLanguage;
	
	public CommandLanguage(Tenyrian plugin) {
		
		this.plugin = plugin;
		menueSetLanguage = new MenueSetLanguage();
	}

	@Override
	public boolean onCommand(Player player, String cmd, String[] args) {
		
		String prefix = "Info";
		String command = "/language";
		
		if(args.length != 0){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.CommandUsage, new String[]{"@cmd=" + command});
			return true;
		}
		
		MenueManager menueManager = TenyrianUtils.getMenueManager();
		menueManager.open(player, menueSetLanguage.getMenue());
		
		return true;
	}

}
