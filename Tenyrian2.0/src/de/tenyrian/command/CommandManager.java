package de.tenyrian.command;

import de.tenyrian.Tenyrian;
import de.tenyrian.chat.ChatManager;
import de.tenyrian.command.admin.CommandAddLevel;
import de.tenyrian.command.admin.CommandAddXP;
import de.tenyrian.command.admin.CommandBroadcast;
import de.tenyrian.command.admin.CommandChatFilter;
import de.tenyrian.command.admin.CommandGamemode;
import de.tenyrian.command.admin.CommandHeal;
import de.tenyrian.command.admin.CommandInvSee;
import de.tenyrian.command.admin.CommandRestart;
import de.tenyrian.command.admin.CommandSetKampfzone;
import de.tenyrian.command.admin.CommandSetKlasse;
import de.tenyrian.command.admin.CommandSetLevel;
import de.tenyrian.command.admin.CommandSetRank;
import de.tenyrian.command.admin.CommandSetSpawn;
import de.tenyrian.command.admin.CommandTp;
import de.tenyrian.command.admin.CommandTpa;
import de.tenyrian.command.player.CommandChat;
import de.tenyrian.command.player.CommandInfo;
import de.tenyrian.command.player.CommandLanguage;

public class CommandManager {

	private ChatManager chatManager;
	private Tenyrian plugin;
	
	public CommandManager(Tenyrian plugin){
		
		this.plugin = plugin;
		
		chatManager = new ChatManager(plugin);
		
		init();
	}
	
	public void init(){
		
		//Admin
		chatManager.registerCommand("/addlevel", new CommandAddLevel(plugin));				//add level
		chatManager.registerCommand("/setlevel", new CommandSetLevel(plugin));				//set Level
		chatManager.registerCommand("/setklasse", new CommandSetKlasse(plugin));			//set Klasse
		chatManager.registerCommand("/addxp", new CommandAddXP(plugin));					//Add xp
		chatManager.registerCommand("/setrank", new CommandSetRank(plugin)); 				//Rang setzen
		chatManager.registerCommand("/setspawn", new CommandSetSpawn(plugin)); 				//Spawn setzen
		chatManager.registerCommand("/setbattlezone", new CommandSetKampfzone(plugin)); 	//Battlezone setzen
		chatManager.registerCommand("/heal", new CommandHeal(plugin));						//heal
		chatManager.registerCommand("/gamemode", "/gm", new CommandGamemode(plugin));		//gamemode
		chatManager.registerCommand("/broadcast", "/bc", new CommandBroadcast(plugin));		//Server broadcast
		chatManager.registerCommand("/tp", new CommandTp(plugin));							//teleport spieler zu spieler
		chatManager.registerCommand("/tpa", new CommandTpa(plugin));						//teleport spieler zu dir
		chatManager.registerCommand("/restart", new CommandRestart(plugin));				//server restart in 30 sekunden
		chatManager.registerCommand("/invsee", new CommandInvSee(plugin));					//Inventar des spielers betrachten
		chatManager.registerCommand("/chatfilter", "/cf", new CommandChatFilter(plugin));	//Blocked Words
		
		//Player
		chatManager.registerCommand("/language", "/lang", new CommandLanguage(plugin));		//Sprache ändern
		chatManager.registerCommand("/info", new CommandInfo(plugin));						//info
		chatManager.registerCommand("/chat", "/ch", new CommandChat(plugin));				//chat
	}
	
	public ChatManager getChatManager(){
		
		return chatManager;
	}
}
