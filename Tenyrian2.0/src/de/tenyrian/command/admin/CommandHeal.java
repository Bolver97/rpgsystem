package de.tenyrian.command.admin;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import de.tenyrian.Tenyrian;
import de.tenyrian.chat.CommandListener;
import de.tenyrian.klasse.Klassen;
import de.tenyrian.language.Chat;
import de.tenyrian.language.LanguageManager;
import de.tenyrian.rank.Rank;
import de.tenyrian.rpgplayer.RpgPlayer;
import de.tenyrian.utils.MathUtils;
import de.tenyrian.utils.PartikelUtils;

public class CommandHeal extends CommandListener{

	private Tenyrian plugin;
	
	public CommandHeal(Tenyrian plugin) {
		
		this.plugin = plugin;
		
		LanguageManager lm = plugin.getLanguageManager();
		
		lm.addDefaultString("heal.player.offline", "Der Spieler @p ist momentan nicht Online!");
		lm.addDefaultString("heal.success.sender", "Du hast @p geheilt!");
		lm.addDefaultString("heal.success.target", "Du wurdest geheilt!");
	}

	@Override
	public boolean onCommand(Player player, String cmd, String[] args) {
		
		String prefix = "Admin";
		String command = "/heal <player>";
		
		if(!Rank.hasPermissions(player, Rank.GROUP_ADMIN)){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.NoPermission, null);
			return true;
		}
		
		if(args.length != 1){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.CommandUsage, new String[]{"@cmd=" + command});
			return true;
		}
		
		Player target = Bukkit.getPlayer(args[0]);
		if(target == null){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, "heal.player.offline", new String[]{"@p=" + args[0]});
			return true;
		}
		
		RpgPlayer rpgPlayer = plugin.getRpgPlayerManager().getRpgPlayer(target);
		rpgPlayer.setFullLife();
		rpgPlayer.setFullMana();
		player.setFoodLevel(20);
		
		if(!player.getName().equals(target.getName())){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, "heal.success.sender", null);
		}
		
		Chat.sendMessage(target, Chat.PREFIX, prefix, "heal.success.target", new String[]{"@p=" + target.getName()});
		return true;
	}

}
