package de.tenyrian.command.admin;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import de.tenyrian.Tenyrian;
import de.tenyrian.chat.CommandListener;
import de.tenyrian.language.Chat;
import de.tenyrian.language.LanguageManager;
import de.tenyrian.rank.Rank;
import de.tenyrian.utils.MathUtils;

public class CommandGamemode extends CommandListener{

	private Tenyrian plugin;
	
	public CommandGamemode(Tenyrian plugin) {
		
		this.plugin = plugin;
		
		LanguageManager lm = plugin.getLanguageManager();
		
		lm.addDefaultString("gamemode.player.offline", "Der Spieler @p ist momentan nicht Online!");
		lm.addDefaultString("gamemode.argument", "Der Gamemode muss zwichen @a liegen!");
		lm.addDefaultString("gamemode.success.sender", "Du hast @p in den Gamemode @a gesetzt!");
		lm.addDefaultString("gamemode.success.target", "Du wurdest in den Gamemode @a gesetzt!");
	}

	@Override
	public boolean onCommand(Player player, String cmd, String[] args) {
		
		String prefix = "Admin";
		String command = "/gamemode <player> <Gamemode>";
		String command02 = "/gamemode <Gamemode>";
		
		int gamemode = 0;
		Player target;
		
		if(!Rank.hasPermissions(player, Rank.GROUP_MODERAOTR)){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.NoPermission, null);
			return true;
		}
		
		if(args.length == 1){
			
			if(!MathUtils.isInteger(args[0])){
				
				Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.CommandUsage, new String[]{"@cmd=" + command02});
				return true;
			}
			
			gamemode = Integer.parseInt(args[0]);
			target = player;
			
		}else if(args.length == 2){
			
			if(!MathUtils.isInteger(args[1])){
				
				Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.CommandUsage, new String[]{"@cmd=" + command});
				return true;
			}
			
			gamemode = Integer.parseInt(args[1]);
			
			Player arg = Bukkit.getPlayer(args[0]);
			if(arg == null){
				
				Chat.sendMessage(player, Chat.PREFIX, prefix, "gamemode.player.offline", new String[]{"@p=" + args[0]});
				return true;
			}
			
			target = arg;
			
		}else{
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.CommandUsage, new String[]{"@cmd=" + command});
			return true;
		}
		
		if(gamemode < 0 || gamemode > 3){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, "gamemode.argument", new String[]{"@a=0-3"});
			return true;
		}
		
		target.setGameMode(GameMode.getByValue(gamemode));
		
		if(!player.getName().equals(target.getName())){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, "gamemode.success.sender", new String[]{"@a=" + GameMode.getByValue(gamemode).name()});
		}
		
		Chat.sendMessage(target, Chat.PREFIX, prefix, "gamemode.success.target", new String[]{"@p=" + target.getName(), "@a=" + GameMode.getByValue(gamemode).name()});
		return true;
	}

}
