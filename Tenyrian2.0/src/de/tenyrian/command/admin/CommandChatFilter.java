package de.tenyrian.command.admin;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import de.tenyrian.Tenyrian;
import de.tenyrian.chat.ChatFilter;
import de.tenyrian.chat.CommandListener;
import de.tenyrian.klasse.Klassen;
import de.tenyrian.language.Chat;
import de.tenyrian.language.LanguageManager;
import de.tenyrian.rank.Rank;
import de.tenyrian.rpgplayer.RpgPlayer;
import de.tenyrian.utils.MathUtils;
import de.tenyrian.utils.PlayerUtils;
import de.tenyrian.utils.TenyrianUtils;

public class CommandChatFilter extends CommandListener{

	private Tenyrian plugin;
	
	public CommandChatFilter(Tenyrian plugin) {
		
		this.plugin = plugin;
		
		LanguageManager lm = plugin.getLanguageManager();
		
		lm.addDefaultString("chatfilter.remove.word.exist", "Das Wort @a existiert nicht!");
		lm.addDefaultString("chatfilter.remove.word.remove", "Das Wort @a wurde entfernt!");
		
		lm.addDefaultString("chatfilter.add.word.exist", "Das Wort @a existiert bereits!");
		lm.addDefaultString("chatfilter.add.word.add", "Das Wort @a wurde hinzugefügt!");
	}

	@Override
	public boolean onCommand(Player player, String cmd, String[] args) {
		
		String prefix = "Admin";
		String command = "/chatfilter <add/remove/(list)> <word/(site)>";
		
		if(!Rank.hasPermissions(player, Rank.GROUP_MODERAOTR)){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.NoPermission, null);
			return true;
		}
		
		if(args.length != 2 && args.length != 1){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.CommandUsage, new String[]{"@cmd=" + command});
			return true;
		}
		
		
		ChatFilter chatFilter = TenyrianUtils.getChatFilter();
		
		switch (args[0].toLowerCase()) {
		case "add":
			
			if(args.length != 2){
				
				Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.CommandUsage, new String[]{"@cmd=" + command});
				return true;
			}
			
			if(chatFilter.existWord(args[1])){
				
				Chat.sendMessage(player, Chat.PREFIX, prefix, "chatfilter.add.word.exist", new String[]{"@a=" + args[1]});
				return true;
			}
			
			chatFilter.addWord(args[1]);
			Chat.sendMessage(player, Chat.PREFIX, prefix, "chatfilter.add.word.add", new String[]{"@a=" + args[1]});
			return true;
		case "remove":
			
			if(args.length != 2){
				
				Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.CommandUsage, new String[]{"@cmd=" + command});
				return true;
			}
			
			if(!chatFilter.existWord(args[1])){
				
				Chat.sendMessage(player, Chat.PREFIX, prefix, "chatfilter.remove.word.exist", new String[]{"@a=" + args[1]});
				return true;
			}
			
			chatFilter.removeWord(args[1]);
			Chat.sendMessage(player, Chat.PREFIX, prefix, "chatfilter.remove.word.remove", new String[]{"@a=" + args[1]});
			return true;
		case "list":
			
			ArrayList<String> words = chatFilter.getWords();
			
			int siteSize = 9;
			int site = 0;
			int siteMax = (int) Math.ceil(words.size()/siteSize);
			
			if(args.length == 2){
				
				if(!MathUtils.isInteger(args[1])){
					
					Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.CommandUsage, new String[]{"@cmd=" + command});
					return true;
				}
				
				site = Integer.parseInt(args[1]);
				
				if(site > siteMax) site = siteMax;
			}
			
			Chat.sendMenue(player, "Blocked Word's " + site + "/" + siteMax, 7);
			for(int i = (siteSize*site); i < (siteSize*(site + 1)) && i < words.size(); i++){
				
				player.sendMessage(words.get(i));
			}
			
			return true;
		default:
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.CommandUsage, new String[]{"@cmd=" + command});
			return true;
		}
	}

}
