package de.tenyrian.command.admin;

import org.bukkit.entity.Player;

import de.tenyrian.Tenyrian;
import de.tenyrian.chat.CommandListener;
import de.tenyrian.klasse.Klassen;
import de.tenyrian.language.Chat;
import de.tenyrian.language.LanguageManager;
import de.tenyrian.rank.Rank;
import de.tenyrian.region.Region;
import de.tenyrian.utils.LocationUtils;
import de.tenyrian.utils.MathUtils;
import de.tenyrian.utils.RegionUtils;

public class CommandSetSpawn extends CommandListener{

	private Tenyrian plugin;
	
	public CommandSetSpawn(Tenyrian plugin) {
		
		this.plugin = plugin;
		
		LanguageManager lm = plugin.getLanguageManager();
		
		lm.addDefaultString("set.spawn.argument", "Die eingegebene Größe muss größer als @a sein!");
		lm.addDefaultString("set.spawn.success.sender", "Du hast hast den Spawn gesetzt!");
	}

	@Override
	public boolean onCommand(Player player, String cmd, String[] args) {
		
		String prefix = "Admin";
		String command = "/setspawn <size>";
		
		if(!Rank.hasPermissions(player, Rank.GROUP_ADMIN)){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.NoPermission, null);
			return true;
		}
		
		if(args.length != 1){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.CommandUsage, new String[]{"@cmd=" + command});
			return true;
		}
		
		if(!MathUtils.isInteger(args[0])){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.CommandUsage, new String[]{"@cmd=" + command});
			return true;
		}
		
		int arg = Integer.parseInt(args[0]);
		if(arg < 1){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, "set.spawn.argument", new String[]{"@a=0"});
			return true;
		}
		
		plugin.getConfig().set("config.spawn.location", LocationUtils.toString(player.getLocation()));
		plugin.saveConfig();
		
		RegionUtils.createSphere("Spawn", player.getLocation(), arg);

		Chat.sendMessage(player, Chat.PREFIX, prefix, "set.spawn.success.sender", null);
		return true;
	}

}
