package de.tenyrian.command.admin;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import de.tenyrian.Tenyrian;
import de.tenyrian.chat.CommandListener;
import de.tenyrian.klasse.Klassen;
import de.tenyrian.language.Chat;
import de.tenyrian.language.LanguageManager;
import de.tenyrian.rank.Rank;
import de.tenyrian.rpgplayer.RpgPlayer;
import de.tenyrian.utils.MathUtils;
import de.tenyrian.utils.PlayerUtils;

public class CommandAddXP extends CommandListener{

	private Tenyrian plugin;
	
	public CommandAddXP(Tenyrian plugin) {
		
		this.plugin = plugin;
		
		LanguageManager lm = plugin.getLanguageManager();
		
		lm.addDefaultString("add.xp.player.offline", "Der Spieler @p ist momentan nicht Online!");
		lm.addDefaultString("add.xp.levelargument", "Die angegebenen XP müssen zwichen @a liegen!");
		lm.addDefaultString("add.xp.success.sender", "Du hast die XP vom Spieler @p um @a erhöht!");
		lm.addDefaultString("add.xp.success.target", "Deine XP wurde um @a erhöht!");
	}

	@Override
	public boolean onCommand(Player player, String cmd, String[] args) {
		
		String prefix = "Admin";
		String command = "/addxp <player> <xp>";
		
		if(!Rank.hasPermissions(player, Rank.GROUP_ADMIN)){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.NoPermission, null);
			return true;
		}
		
		if(args.length != 2){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.CommandUsage, new String[]{"@cmd=" + command});
			return true;
		}
		
		if(!MathUtils.isInteger(args[1])){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.CommandUsage, new String[]{"@cmd=" + command});
			return true;
		}
		
		Player target = Bukkit.getPlayer(args[0]);
		if(target == null){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, "add.xp.player.offline", new String[]{"@p=" + args[0]});
			return true;
		}
		
		RpgPlayer rpgPlayer = PlayerUtils.getRpgPlayer(target);
		
		int xp = Integer.parseInt(args[1]);
		
		if(xp < 1 || xp > 1000000){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, "add.xp.levelargument", new String[]{"@a=1-1000000"});
			return true;
		}
		
		rpgPlayer.addXP(xp);
		
		if(!player.getName().equals(target.getName())){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, "add.xp.success.sender", new String[]{"@a=" + xp, "@p=" + target.getName()});
		}
		Chat.sendMessage(target, Chat.PREFIX, prefix, "add.xp.success.target", new String[]{"@a=" + xp});
		return true;
	}

}
