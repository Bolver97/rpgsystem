package de.tenyrian.command.admin;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import de.tenyrian.Tenyrian;
import de.tenyrian.chat.CommandListener;
import de.tenyrian.language.Chat;
import de.tenyrian.language.LanguageManager;
import de.tenyrian.rank.Rank;
import de.tenyrian.utils.MathUtils;

public class CommandTpa extends CommandListener{

	private Tenyrian plugin;
	
	public CommandTpa(Tenyrian plugin) {
		
		this.plugin = plugin;
		
		LanguageManager lm = plugin.getLanguageManager();
		
		lm.addDefaultString("tpa.player.offline", "Der Spieler @p ist momentan nicht Online!");
		lm.addDefaultString("tpa.success.sender", "Du hast @p zu dir telepotiert!");
		lm.addDefaultString("tpa.success.target", "Du wurdest zum Spieler @p teleportiert!");
	}

	@Override
	public boolean onCommand(Player player, String cmd, String[] args) {
		
		String prefix = "Admin";
		String command = "/tpa <player>";
		
		Player to;
		
		if(!Rank.hasPermissions(player, Rank.GROUP_SUPPORTER)){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.NoPermission, null);
			return true;
		}
		
		if(args.length == 1){
			
			to = Bukkit.getPlayer(args[0]);
			if(to == null){
				
				Chat.sendMessage(player, Chat.PREFIX, prefix, "tpa.player.offline", new String[]{"@p=" + command});
				return true;				
			}
			
			to.teleport(player);
			Chat.sendMessage(player, Chat.PREFIX, prefix, "tpa.success.sender", new String[]{"@p=" + to.getName()});
			Chat.sendMessage(to, Chat.PREFIX, prefix, "tpa.success.target", new String[]{"@p=" + player.getName()});
			return true;	
			
		}else{
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.CommandUsage, new String[]{"@cmd=" + command});
			return true;
		}
	}

}
