package de.tenyrian.command.admin;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import de.tenyrian.Tenyrian;
import de.tenyrian.chat.CommandListener;
import de.tenyrian.klasse.Klassen;
import de.tenyrian.language.Chat;
import de.tenyrian.language.LanguageManager;
import de.tenyrian.rank.Rank;
import de.tenyrian.rpgplayer.RpgPlayer;
import de.tenyrian.utils.MathUtils;

public class CommandSetLevel extends CommandListener{

	private Tenyrian plugin;
	
	public CommandSetLevel(Tenyrian plugin) {
		
		this.plugin = plugin;
		
		LanguageManager lm = plugin.getLanguageManager();
		
		lm.addDefaultString("set.level.player.offline", "Der Spieler @p ist momentan nicht Online!");
		lm.addDefaultString("set.level.levelargument", "Das eingegebene Level muss zwichen @a liegen!");
		lm.addDefaultString("set.level.success.sender", "Du hast das Level vom Spieler @p auf @a gesetzt!");
		lm.addDefaultString("set.level.success.target", "Dein Level wurde auf @a gesetzt!");
	}

	@Override
	public boolean onCommand(Player player, String cmd, String[] args) {
		
		String prefix = "Admin";
		String command = "/setlevel <player> <level>";
		
		if(!Rank.hasPermissions(player, Rank.GROUP_ADMIN)){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.NoPermission, null);
			return true;
		}
		
		if(args.length != 2){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.CommandUsage, new String[]{"@cmd=" + command});
			return true;
		}
		
		if(!MathUtils.isInteger(args[1])){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.CommandUsage, new String[]{"@cmd=" + command});
			return true;
		}
		
		Player target = Bukkit.getPlayer(args[0]);
		if(target == null){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, "set.level.player.offline", new String[]{"@p=" + args[0]});
			return true;
		}
		
		int level = Integer.parseInt(args[1]);
		if(level < 1 || level > Klassen.MaxLevel){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, "set.level.levelargument", new String[]{"@a=1-" + Klassen.MaxLevel});
			return true;
		}
		
		RpgPlayer rpgPlayer = plugin.getRpgPlayerManager().getRpgPlayer(target);
		rpgPlayer.setLevel(level);
		
		if(!player.getName().equals(target.getName())){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, "set.level.success.sender", new String[]{"@a=" + level, "@p=" + target.getName()});
		}
		Chat.sendMessage(target, Chat.PREFIX, prefix, "set.level.success.target", new String[]{"@a=" + level});
		return true;
	}

}
