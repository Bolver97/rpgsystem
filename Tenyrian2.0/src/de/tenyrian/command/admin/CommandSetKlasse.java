package de.tenyrian.command.admin;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import de.tenyrian.Tenyrian;
import de.tenyrian.chat.CommandListener;
import de.tenyrian.klasse.Klassen;
import de.tenyrian.language.Chat;
import de.tenyrian.language.LanguageManager;
import de.tenyrian.rank.Rank;
import de.tenyrian.rpgplayer.RpgPlayer;
import de.tenyrian.utils.MathUtils;
import de.tenyrian.utils.PlayerUtils;
import de.tenyrian.utils.TablistUtils;

public class CommandSetKlasse extends CommandListener{

	private Tenyrian plugin;
	
	public CommandSetKlasse(Tenyrian plugin) {
		
		this.plugin = plugin;
		
		
		LanguageManager lm = plugin.getLanguageManager();
		
		lm.addDefaultString("set.klasse.player.offline", "Der Spieler @p ist momentan nicht Online!");
		lm.addDefaultString("set.klasse.rankargument", "Die eingegebene Klasse @a existiert nicht!");
		lm.addDefaultString("set.klasse.success.sender", "Du hast die Klasse von @p zu @a geändert!");
		lm.addDefaultString("set.klasse.success.target", "Deine Klasse wurde zu @a geändert!");
	}

	@Override
	public boolean onCommand(Player player, String cmd, String[] args) {
		
		String prefix = "Admin";
		String command = "/setklasse <player> <klasse>";
		
		if(!Rank.hasPermissions(player, Rank.GROUP_ADMIN)){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.NoPermission, null);
			return true;
		}
		
		if(args.length != 2){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.CommandUsage, new String[]{"@cmd=" + command});
			return true;
		}
		
		int klasse;
		switch (args[1].toLowerCase()) {
		case "schattenklinge":
			klasse = Klassen.ROUGE;
			break;
		case "waldläufer":
			klasse = Klassen.HUNTER;
			break;
		case "templer":
			klasse = Klassen.KNIGHT;
			break;
		case "hexer":
			klasse = Klassen.WITCH;
			break;
		case "barbar":
			klasse = Klassen.BARBAR;
			break;
		default:
			klasse = -1;
			break;
		}
		
		if(klasse == -1){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, "set.klasse.rankargument", new String[]{"@a=" + args[1]});
			return true;
		}
		
		Player target = Bukkit.getPlayer(args[0]);
		if(target == null){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, "set.klasse.player.offline", new String[]{"@p=" + args[0]});
			return true;
		}
		
		RpgPlayer rpgPlayer = PlayerUtils.getRpgPlayer(target);
		rpgPlayer.changeKlass(klasse);
		
		TablistUtils.update();
		
		if(!player.getName().equals(target.getName())){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, "set.klasse.success.sender", new String[]{"@a=" + args[1], "@p=" + target.getName()});
		}
		Chat.sendMessage(target, Chat.PREFIX, prefix, "set.klasse.success.target", new String[]{"@a=" + Klassen.getKlassenName(rpgPlayer.getLanguage(), klasse)});
		return true;
	}

}
