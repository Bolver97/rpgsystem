package de.tenyrian.command.admin;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import de.tenyrian.Tenyrian;
import de.tenyrian.chat.CommandListener;
import de.tenyrian.language.Chat;
import de.tenyrian.language.LanguageManager;
import de.tenyrian.rank.Rank;
import de.tenyrian.utils.MathUtils;

public class CommandTp extends CommandListener{

	private Tenyrian plugin;
	
	public CommandTp(Tenyrian plugin) {
		
		this.plugin = plugin;
		
		LanguageManager lm = plugin.getLanguageManager();
		
		lm.addDefaultString("tp.player.offline", "Der Spieler @p ist momentan nicht Online!");
		lm.addDefaultString("tp.success.sender", "Du hast @p1 zu @p2 telepotiert!");
		lm.addDefaultString("tp.success.target", "Du wurdest zum Spieler @p teleportiert!");
		lm.addDefaultString("tp.success.sender", "Du hast dich zum Spieler @p teleportiert!");
	}

	@Override
	public boolean onCommand(Player player, String cmd, String[] args) {
		
		String prefix = "Admin";
		String command = "/tp <player> <player>";
		String command02 = "/tp <player>";
		
		Player from;
		Player to;
		
		if(!Rank.hasPermissions(player, Rank.GROUP_SUPPORTER)){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.NoPermission, null);
			return true;
		}
		
		if(args.length == 1){
			
			to = Bukkit.getPlayer(args[0]);
			if(to == null){
				
				Chat.sendMessage(player, Chat.PREFIX, prefix, "tp.player.offline", new String[]{"@p=" + command});
				return true;				
			}
			
			player.teleport(to);
			Chat.sendMessage(player, Chat.PREFIX, prefix, "tp.success.target", new String[]{"@p=" + to.getName()});
			return true;	
			
		}else if(args.length == 2){

			from = Bukkit.getPlayer(args[0]);
			if(from == null){
				
				Chat.sendMessage(player, Chat.PREFIX, prefix, "tp.player.offline", new String[]{"@p=" + args[0]});
				return true;				
			}
			
			to = Bukkit.getPlayer(args[1]);
			if(to == null){
				
				Chat.sendMessage(player, Chat.PREFIX, prefix, "tp.player.offline", new String[]{"@p=" + args[1]});
				return true;				
			}

			from.teleport(to);
			if(from.getName().equals(player.getName())){
				
				Chat.sendMessage(from, Chat.PREFIX, prefix, "tp.success.sender.target", new String[]{"@p=" + to.getName()});
				return true;
			}else{
				
				Chat.sendMessage(player, Chat.PREFIX, prefix, "tp.success.sender", new String[]{"@p1=" + from.getName(), "@p2=" + to.getName()});
				Chat.sendMessage(from, Chat.PREFIX, prefix, "tp.success.target", new String[]{"@p=" + to.getName()});
				return true;
			}	
			
		}else{
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.CommandUsage, new String[]{"@cmd=" + command});
			return true;
		}
	}

}
