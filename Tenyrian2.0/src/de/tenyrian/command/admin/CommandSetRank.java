package de.tenyrian.command.admin;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import de.tenyrian.Tenyrian;
import de.tenyrian.chat.CommandListener;
import de.tenyrian.klasse.Klassen;
import de.tenyrian.language.Chat;
import de.tenyrian.language.LanguageManager;
import de.tenyrian.rank.Rank;
import de.tenyrian.rpgplayer.RpgPlayer;
import de.tenyrian.utils.MathUtils;

public class CommandSetRank extends CommandListener{

	private Tenyrian plugin;
	
	public CommandSetRank(Tenyrian plugin) {
		
		this.plugin = plugin;
		
		
		LanguageManager lm = plugin.getLanguageManager();
		
		lm.addDefaultString("set.rank.player.offline", "Der Spieler @p ist momentan nicht Online!");
		lm.addDefaultString("set.rank.rankargument", "Der eingegebene Rank @a existiert nicht!");
		lm.addDefaultString("set.rank.success.sender", "Du hast den Rang von @p zu @a geändert!");
		lm.addDefaultString("set.rank.success.target", "Dein Rang wurde zu @a geändert!");
	}

	@Override
	public boolean onCommand(Player player, String cmd, String[] args) {
		
		String prefix = "Admin";
		String command = "/setrank <player> <rank>";
		
		if(!Rank.hasPermissions(player, Rank.GROUP_ADMIN)){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.NoPermission, null);
			return true;
		}
		
		if(args.length != 2){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.CommandUsage, new String[]{"@cmd=" + command});
			return true;
		}
		
		int rank;
		switch (args[1].toLowerCase()) {
		case "headadmin":
			rank = Rank.HEAD_ADMIN;
			break;
		case "admin":
			rank = Rank.ADMIN;
			break;
		case "moderator":
			rank = Rank.MODERATOR;
			break;
		case "developer":
			rank = Rank.DEVELOPER;
			break;
		case "supporter":
			rank = Rank.SUPPORTER;
			break;
		case "grafiker":
			rank = Rank.GRAFIKER;
			break;
		case "konzeptler":
			rank = Rank.KONZEPTLER;
			break;
		case "prmanager":
			rank = Rank.PR_MANAGER;
			break;
		case "architekt":
			rank = Rank.ARCHITEKT;
			break;
		case "spielerdiamant":
			rank = Rank.SPIELER_DIAMANT;
			break;
		case "spielersilber":
			rank = Rank.SPIELER_SILBER;
			break;
		case "spielerbronze":
			rank = Rank.SPIELER_BRONZE;
			break;
		case "spieler":
			rank = Rank.SPIELER;
			break;
		default:
			rank = -1;
			break;
		}
		
		if(rank == -1){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, "set.rank.rankargument", new String[]{"@a=" + args[1]});
			return true;
		}
		
		Player target = Bukkit.getPlayer(args[0]);
		if(target == null){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, "set.rank.player.offline", new String[]{"@p=" + args[0]});
			return true;
		}
		
		RpgPlayer rpgPlayer = plugin.getRpgPlayerManager().getRpgPlayer(target);
		rpgPlayer.setRank(rank);
		
		if(!player.getName().equals(target.getName())){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, "set.rank.success.sender", new String[]{"@a=" + args[1], "@p=" + target.getName()});
		}
		Chat.sendMessage(target, Chat.PREFIX, prefix, "set.rank.success.target", new String[]{"@a=" + args[1]});
		return true;
	}

}
