package de.tenyrian.command.admin;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import de.tenyrian.Tenyrian;
import de.tenyrian.chat.CommandListener;
import de.tenyrian.klasse.Klassen;
import de.tenyrian.language.Chat;
import de.tenyrian.language.LanguageManager;
import de.tenyrian.menue.MenueListener;
import de.tenyrian.menue.player.MenueInvSee;
import de.tenyrian.rank.Rank;
import de.tenyrian.rpgplayer.RpgPlayer;
import de.tenyrian.utils.MathUtils;
import de.tenyrian.utils.PlayerUtils;
import de.tenyrian.utils.TablistUtils;
import de.tenyrian.utils.TenyrianUtils;

public class CommandInvSee extends CommandListener{

	private Tenyrian plugin;
	
	public CommandInvSee(Tenyrian plugin) {
		
		this.plugin = plugin;
		
		LanguageManager lm = plugin.getLanguageManager();
		
		lm.addDefaultString("invsee.player.offline", "Der Spieler @p ist momentan nicht Online!");
		lm.addDefaultString("invsee.success.sender", "Du hast das Inventar von @p geöffnet!");
	}

	@Override
	public boolean onCommand(Player player, String cmd, String[] args) {
		
		String prefix = "Admin";
		String command = "/invsee <player>";
		
		if(!Rank.hasPermissions(player, Rank.GROUP_MODERAOTR)){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.NoPermission, null);
			return true;
		}
		
		if(args.length != 1){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.CommandUsage, new String[]{"@cmd=" + command});
			return true;
		}
		
		Player target = Bukkit.getPlayer(args[0]);
		if(target == null){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, "invsee.player.offline", new String[]{"@p=" + args[0]});
			return true;
		}
		
		MenueListener MenueInvSee = new MenueInvSee(target);
		TenyrianUtils.getMenueManager().open(player, MenueInvSee.getMenue());
		
		Chat.sendMessage(player, Chat.PREFIX, prefix, "invsee.success.sender", new String[]{"@p=" + target.getName()});
		return true;
	}

}
