package de.tenyrian.command.admin;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import de.tenyrian.Tenyrian;
import de.tenyrian.chat.CommandListener;
import de.tenyrian.language.Chat;
import de.tenyrian.language.LanguageManager;
import de.tenyrian.rank.Rank;
import de.tenyrian.utils.MathUtils;
import de.tenyrian.utils.PlayerUtils;
import de.tenyrian.utils.TitleUtils;

public class CommandRestart extends CommandListener{

	private Tenyrian plugin;
	
	public CommandRestart(Tenyrian plugin) {
		
		this.plugin = plugin;
		
		LanguageManager lm = plugin.getLanguageManager();
		
		lm.addDefaultString("restart.message", "Restart in @a Sekunden!");
		lm.addDefaultString("restart.success.sender", "Der Restart Countdown wurde gestartet!");
	}

	@Override
	public boolean onCommand(Player player, String cmd, String[] args) {
		
		String prefix = "Admin";
		String command = "/restart";
		
		int gamemode = 0;
		Player target;
		
		if(!Rank.hasPermissions(player, Rank.GROUP_MODERAOTR)){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.NoPermission, null);
			return true;
		}
		
		if(args.length != 0){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.CommandUsage, new String[]{"@cmd=" + command});
			return true;	
		}
		
		Chat.sendMessage(player, Chat.PREFIX, prefix, "restart.success.sender", null);
		
		new BukkitRunnable() {
			
			int time = 60;
			
	        @Override
	        public void run() {
	        	
	        	if(time == 0){
	        		
	        		Bukkit.getServer().shutdown();
	        		cancel();
	        	}
	        	
	        	if(time == 60){
	        		
		    		for(Player onlinePlayer : Bukkit.getServer().getOnlinePlayers()){
		    			
		    			TitleUtils.create(onlinePlayer, "§8[§4Server§8]", Chat.getMessage(player, "restart.message",  new String[]{"@a=" + time}, "§c", "§f"));
		    		}
	        	}
	        	
	        	if(time == 30){
	        		
		    		for(Player onlinePlayer : Bukkit.getServer().getOnlinePlayers()){
		    			
		    			TitleUtils.create(onlinePlayer, "§8[§4Server§8]", Chat.getMessage(player, "restart.message",  new String[]{"@a=" + time}, "§c", "§f"));
		    		}
	        	}
	        	
	        	if(time <= 10){
	        		
		    		for(Player onlinePlayer : Bukkit.getServer().getOnlinePlayers()){
		    			
		    			TitleUtils.create(onlinePlayer, "§8[§4Server§8]", Chat.getMessage(player, "restart.message",  new String[]{"@a=" + time}, "§c", "§f"));
		    		}
	        	}
	    		
	    		time--;
	        }
	    }.runTaskTimer(plugin, 0L, 20L);   
	    
		return true;
	}

}
