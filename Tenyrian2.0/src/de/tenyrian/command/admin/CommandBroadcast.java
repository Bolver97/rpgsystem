package de.tenyrian.command.admin;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import de.tenyrian.Tenyrian;
import de.tenyrian.chat.CommandListener;
import de.tenyrian.language.Chat;
import de.tenyrian.language.LanguageManager;
import de.tenyrian.rank.Rank;
import de.tenyrian.utils.MathUtils;
import de.tenyrian.utils.TitleUtils;

public class CommandBroadcast extends CommandListener{

	private Tenyrian plugin;
	
	public CommandBroadcast(Tenyrian plugin) {
		
		this.plugin = plugin;
		
		LanguageManager lm = plugin.getLanguageManager();
		
		lm.addDefaultString("broadcast.success.sender", "Dein Broadcast wurde gesendet!");
	}

	@Override
	public boolean onCommand(Player player, String cmd, String[] args) {
		
		String prefix = "Admin";
		String command = "/broadcast <message>";
		
		int gamemode = 0;
		Player target;
		
		if(!Rank.hasPermissions(player, Rank.GROUP_MODERAOTR)){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.NoPermission, null);
			return true;
		}
		
		if(args.length == 0){
			
			Chat.sendMessage(player, Chat.PREFIX, prefix, Chat.CommandUsage, new String[]{"@cmd=" + command});
			return true;	
		}
		
		Chat.sendMessage(player, Chat.PREFIX, prefix, "broadcast.success.sender", null);
		
		for(Player onlinePlayer : Bukkit.getServer().getOnlinePlayers()){
			
			TitleUtils.create(onlinePlayer, "§8[§4Server§8]", "§c" + StringUtils.join(args, " "));
		}
		return true;
	}

}
