package de.tenyrian.region;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import org.bukkit.event.player.PlayerMoveEvent;

import de.tenyrian.Tenyrian;
import de.tenyrian.sql.SqlConnection;
import de.tenyrian.utils.LocationUtils;

public class RegionManager implements Listener {

	private HashMap<Player, ArrayList<Region>> regionOldPlayer = new HashMap<>(); 
	private HashMap<Player, ArrayList<Region>> regionPlayer = new HashMap<>(); 
	private ArrayList<Region> regionList;
	
	private Tenyrian plugin;
	
	public RegionManager(Tenyrian plugin) {
		
		this.plugin = plugin;
		
		load();
		
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		
		for(Player player : Bukkit.getServer().getOnlinePlayers()){
			
			update(player);
		}
	}
	
	public void load(){
		
		regionList = new ArrayList<>();
		
		SqlConnection sqlConnection = plugin.getSqlConnection();
		
        try {
            PreparedStatement request = sqlConnection.getConnection().prepareStatement("SELECT * From `Region`");
            ResultSet result = request.executeQuery();
            while(result.next()){
            	
	            String name = result.getString(2);
	            int type = result.getInt(3);
	            String position = result.getString(4);
	            
	            regionList.add(new Region(name, type, position));
            }
            request.close();
            result.close();
            
        } catch (SQLException e) {
        	
        	Bukkit.getLogger().info("SQL select Failed" + e.toString());
        }
	}
	
	public void addSphere(String name, Location loc, int size){
		
		Region region = new Region(name, RegionType.SPHERE, LocationUtils.toString(loc) + ":" + size);
		region.create();
		regionList.add(region);
	}
	
	public void addChunk(String name, Location loc){
		
		Region region = new Region(name, RegionType.CHUNK, LocationUtils.toString(loc));
		region.create();
		regionList.add(region);
	}
	
	public void addRegion(String name, Location loc01, Location loc02){
		
		Region region = new Region(name, RegionType.REGION, LocationUtils.toString(loc01) + ":" + LocationUtils.toString(loc02));
		region.create();		
		regionList.add(region);
	}
	
	public void remove(String name){
		
		Region removedRegion = null;
		
		for(Region region : regionList){
			
			if(region.getName().equals(name)){
				removedRegion = region;
			}
		}
		
		if(removedRegion != null){
			
			regionList.remove(removedRegion);
			removedRegion.delete();
		}
		
		for(Player player : Bukkit.getServer().getOnlinePlayers()){
			
			update(player);
		}
	}
	
	public ArrayList<Region> getCollisions(Player player){
		
		if(regionPlayer.containsKey(player)){
			
			return regionPlayer.get(player);
		}else{
			
			return new ArrayList<Region>();
		}
	}
	
	public ArrayList<Region> getCollisions(Location location){
		
		ArrayList<Region> regions = new ArrayList<>();
		
		for(Region region : regionList){
			
			if(region.isColidingWith(location)){
				
				regions.add(region);
			}
		}
		
		return regions;
	}
	
	public void update(Player player){
		
		ArrayList<Region> regions = new ArrayList<>();
		
		for(Region region : regionList){
			
			if(region.isColidingWith(player.getLocation())){
				
				regions.add(region);
			}
		}
		
		regionPlayer.put(player, regions);
	}
}
