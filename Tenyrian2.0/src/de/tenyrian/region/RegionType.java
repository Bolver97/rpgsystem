package de.tenyrian.region;

public interface RegionType {

	int SPHERE = 2;
	int CHUNK = 1;
	int REGION = 0;
}
