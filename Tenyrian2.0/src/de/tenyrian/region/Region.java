package de.tenyrian.region;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;

import de.tenyrian.Tenyrian;
import de.tenyrian.klasse.Klassen;
import de.tenyrian.sql.SqlConnection;
import de.tenyrian.utils.LocationUtils;

public class Region {

	private String name;
	private int type;
	private Location loc01;
	private Location loc02;
	private int size;
	
	private String position;
	
	public Region(String name, int type, String position) {

		this.name = name;
		this.type = type;
		this.position = position;
		
		String[] positionSplit;
		
		switch (type) {
		case RegionType.CHUNK:
			
			this.loc01 = LocationUtils.toLoc(position);
			break;
		case RegionType.REGION:
			
			positionSplit = position.split(":");
			
			this.loc01 = LocationUtils.toLoc(positionSplit[0]);
			this.loc02 = LocationUtils.toLoc(positionSplit[1]);
			break;
		case RegionType.SPHERE:
			
			positionSplit = position.split(":");
			this.loc01 = LocationUtils.toLoc(positionSplit[0]);
			this.size = Integer.parseInt(positionSplit[1]);
			break;
		}
	}
	
	public boolean isColidingWith(Location eloc){
		
		Location entityLocation = eloc;
		
		switch (type) {
		case RegionType.CHUNK:
			
			if(entityLocation.getChunk().equals(loc01.getChunk())) return true;
			break;
		case RegionType.REGION:
			
			float eX = (float) entityLocation.getX();
			float eY = (float) entityLocation.getY();
			float eZ = (float) entityLocation.getZ();
			
			float maxX = (float) Math.max(loc01.getX(), loc02.getX());
			float maxY = (float) Math.max(loc01.getY(), loc02.getY());
			float maxZ = (float) Math.max(loc01.getZ(), loc02.getZ());
			
			float minX = (float) Math.min(loc01.getX(), loc02.getX());
			float minY = (float) Math.min(loc01.getY(), loc02.getY());
			float minZ = (float) Math.min(loc01.getZ(), loc02.getZ());
			
			if(eX >= minX && eX <= maxX){
				if(eY >= minY && eY <= maxY){
					if(eZ >= minZ && eZ <= maxZ){
						
						return true;
					}					
				}				
			}
			
			break;
		case RegionType.SPHERE:

			if(entityLocation.toVector().distance(loc01.toVector()) <= size) return true;
			break;
		}
		return false;
	}
	
	public String getName(){
		return name;
	}

	public void create(){
		
		SqlConnection sqlConnection = Tenyrian.getInstance().getSqlConnection();

        try {
        	
        	PreparedStatement insert = sqlConnection.getConnection().prepareStatement("INSERT INTO `Region` values(0,?,?,?);");
            insert.setString(1, name);
            insert.setInt(2, type);
            insert.setString(3, position);
            insert.executeUpdate();
            insert.close();
        } catch (SQLException e) {
        	
             Bukkit.getLogger().info("SQL insert" + e.toString());
        }
	}
	
	public void delete(){
		
		SqlConnection sqlConnection = Tenyrian.getInstance().getSqlConnection();
        try {
        	PreparedStatement update = sqlConnection.getConnection().prepareStatement("DELETE FROM `Region` WHERE Name=? AND Positions=?;");
            update.setString(1, name);
            update.setString(2, position);
            
            update.executeUpdate();
            update.close();
            
        } catch (SQLException e) {
            Bukkit.getLogger().info("SQL remove Failed" + e.toString());
        }
	}
}
