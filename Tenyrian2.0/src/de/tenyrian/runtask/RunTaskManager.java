package de.tenyrian.runtask;

import org.bukkit.scheduler.BukkitRunnable;

import de.tenyrian.Tenyrian;
import de.tenyrian.utils.PlayerUtils;
import de.tenyrian.utils.RegionUtils;

public class RunTaskManager {

	private Tenyrian plugin;
	
	public RunTaskManager(Tenyrian plugin){
		
		this.plugin = plugin;
		
		init();
	}
	
	public void init(){
		
		//1Second Timer
		new BukkitRunnable() {
			
	        @Override
	        public void run() {
	        	
	        	PlayerUtils.doTick();
	        }
	    }.runTaskTimer(plugin, 0L, 20L);   
	    
		//1Second Timer
		new BukkitRunnable() {
			
	        @Override
	        public void run() {
	        	
	        	RegionUtils.update();
	        }
	    }.runTaskTimer(plugin, 0L, 10L);   
	}
}
