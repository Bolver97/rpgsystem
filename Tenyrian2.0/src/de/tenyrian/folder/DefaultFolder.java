package de.tenyrian.folder;

import java.io.File;

import de.tenyrian.Tenyrian;

public class DefaultFolder {

	private Tenyrian plugin;
	
	public DefaultFolder(Tenyrian plugin){
		
		this.plugin = plugin;
		
		load();
	}
	
	public void load(){
		
		addFolder("language");
	}
	
	public void addFolder(String dir){
		
		new File(plugin.getDataFolder(), dir).mkdirs();
	}
}
