package de.tenyrian.rpgplayer;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import de.tenyrian.Tenyrian;
import de.tenyrian.damage.Damage;
import de.tenyrian.klasse.Klassen;
import de.tenyrian.utils.TablistUtils;

public class RpgPlayerManager implements Listener {

	private ArrayList<RpgPlayer> rpgPlayers;
	
	private Tenyrian plugin;
	
	public RpgPlayerManager(Tenyrian plugin) {

		this.plugin = plugin;
		
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		
		init();
	}
	
	public void init(){
		
		rpgPlayers = new ArrayList<>();
		
		for(Player player : Bukkit.getServer().getOnlinePlayers()){
			
			rpgPlayers.add(new RpgPlayer(player));
		}
	}
	
	public void save(){
		
		for(RpgPlayer rpgPlayer : rpgPlayers){
			
			rpgPlayer.save();
		}
	}
	
    @EventHandler (priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent event)
    {
    	
    	Player player = event.getPlayer();
    	if(player !=null){
    		
	    	RpgPlayer rpgPlayer = new RpgPlayer(player);
	    	
	    	rpgPlayers.add(rpgPlayer);
    	}
    	
    	TablistUtils.update();
    }
    
    @EventHandler (priority = EventPriority.HIGHEST)
    public void onPlayerLeave(PlayerQuitEvent event)
    {
    	
    	Player player = event.getPlayer();
    	
    	RpgPlayer rpgPlayer = getRpgPlayer(player);
    	rpgPlayer.save();
    	
    	rpgPlayers.remove(rpgPlayer);
    	
    	plugin.getTablist().update(player.getUniqueId());
    }
    
    @EventHandler (priority = EventPriority.HIGHEST)
    public void onDamage(EntityDamageEvent event)
    {
    	if (event instanceof EntityDamageByEntityEvent) {
    		
    	    EntityDamageByEntityEvent edbeEvent = (EntityDamageByEntityEvent)event;
    	    Entity damager = edbeEvent.getDamager();
    	    Entity damage = event.getEntity();
    	    
        	if(damage instanceof Player && damager instanceof Player){
        		
        		Damage.onPlayerByPlayer(event);
        	}else if(damage instanceof Player){
        		
        		Damage.onPlayerByEntity(event);
        	}else if(damager instanceof Player){
        		
        		Damage.onEntityByPlayer(event);
        	}else{
        		
        		Damage.onEntityByEntity(event);
        	}
    	}else{
    		
    		if(event.getEntity() instanceof Player){
    		
    			Damage.onOtherByEntity(event);
    		}else{
    			
    			Damage.onOtherByPlayer(event);
    		}
    	}
    }
    
    public RpgPlayer getRpgPlayer(UUID uuid){
    	
    	for(RpgPlayer rpgPlayer : rpgPlayers){
    		
    		if(rpgPlayer.getUUID().equals(uuid)) return rpgPlayer;
    	}
    	
		return null;
    }
    
    public RpgPlayer getRpgPlayer(Player player){
    	
    	return getRpgPlayer(player.getUniqueId());
    }

}
