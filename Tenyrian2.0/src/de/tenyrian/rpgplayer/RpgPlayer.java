package de.tenyrian.rpgplayer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.util.Vector;

import de.tenyrian.Tenyrian;
import de.tenyrian.klasse.Klasse;
import de.tenyrian.klasse.Klassen;
import de.tenyrian.language.Chat;
import de.tenyrian.rank.Rank;
import de.tenyrian.sql.SqlConnection;
import de.tenyrian.utils.ActionBarUtils;
import de.tenyrian.utils.LocationUtils;
import de.tenyrian.utils.PartikelUtils;
import de.tenyrian.utils.PlayerUtils;
import de.tenyrian.utils.RegionUtils;
import de.tenyrian.utils.TablistUtils;
import de.tenyrian.utils.TitleUtils;

public class RpgPlayer {

	private UUID uuid;
	private String name;
	private int rank;
	private int klass;
	private int money;
	private int bank;
	private int level = 0;
	private int xp;
	private int life;
	private int mana;
	private Location home;
	private Location logout;
	private String language;
	
	public RpgPlayer(UUID uuid){
		
		this.uuid = uuid;
		
		load();
	}
	
	public RpgPlayer(Player player){
	
        if(!exist(player)) create(player);
        
        this.uuid = player.getUniqueId();
        
        load();
	}
	
	public boolean exist(Player player){
		
		Tenyrian plugin = Tenyrian.getInstance();
		SqlConnection sqlConnection = Tenyrian.getInstance().getSqlConnection();
		
        try {
            PreparedStatement request = sqlConnection.getConnection().prepareStatement("SELECT * From `Player` WHERE UUID=?");
            request.setString(1, player.getUniqueId().toString());
            ResultSet result = request.executeQuery();
            if (!result.next()) {
            	
                return false;
            }else{
            	
            	return true;
            }

        } catch (SQLException e) {
        	
        	return false;
        }
	}
	
	public void create(Player player){
		
		Tenyrian plugin = Tenyrian.getInstance();
		SqlConnection sqlConnection = Tenyrian.getInstance().getSqlConnection();
		FileConfiguration config = plugin.getConfig();
		
        try {
        	
        	PreparedStatement insert = sqlConnection.getConnection().prepareStatement("INSERT INTO `Player` values(0,?,?,?,?,?,?,?,?,?,?,?);");
            insert.setString(1, player.getUniqueId().toString());// UUID
            insert.setString(2, player.getName());// Name
            insert.setInt(3, config.getInt("config.player.rank"));// Rank
            insert.setInt(4, Klassen.NONE);// Class
            insert.setInt(5, config.getInt("config.player.money"));// Money
            insert.setInt(6, config.getInt("config.player.bank"));// Bank
            insert.setString(7, config.getString("config.spawn.location"));// home
            insert.setString(8, config.getString("config.spawn.location"));// logout
            insert.setInt(9, Klassen.getMaxLife(Klassen.NONE, 1));// Life
            insert.setInt(10, Klassen.getMaxMana(Klassen.NONE, 1));// Mana
            insert.setString(11, config.getString("config.player.language"));// Language
            insert.executeUpdate();
            insert.close();
        } catch (SQLException e) {
        	
             Bukkit.getLogger().info("SQL insert" + e.toString());
        }
	}
	
	public boolean existKlass(int klass){
		
		Tenyrian plugin = Tenyrian.getInstance();
		SqlConnection sqlConnection = Tenyrian.getInstance().getSqlConnection();
		
        try {
            PreparedStatement request = sqlConnection.getConnection().prepareStatement("SELECT * From `Klassen` WHERE UUID=? AND Class=?");
            request.setString(1, getUUID().toString());
            request.setInt(2, klass);
            ResultSet result = request.executeQuery();
            if (!result.next()) {
            	
                return false;
            }else{
            	
            	return true;
            }

        } catch (SQLException e) {
        	
        	return false;
        }
	}
	
	public void createKlass(int klass){
		
		Tenyrian plugin = Tenyrian.getInstance();
		SqlConnection sqlConnection = Tenyrian.getInstance().getSqlConnection();
		
        try {
        	
        	PreparedStatement insert = sqlConnection.getConnection().prepareStatement("INSERT INTO `Klassen` values(0,?,?,?,?);");
            insert.setString(1, getUUID().toString());// UUID
            insert.setInt(2, klass);// Klasse
            insert.setInt(3, plugin.getConfig().getInt("config.player.level", 1));// Level
            insert.setInt(4, 0);// Xp
            insert.executeUpdate();
            insert.close();
        } catch (SQLException e) {
        	
             Bukkit.getLogger().info("SQL insert" + e.toString());
        }
	}
	
	public void load(){
		
		Tenyrian plugin = Tenyrian.getInstance();
		SqlConnection sqlConnection = Tenyrian.getInstance().getSqlConnection();
		
        try {
            PreparedStatement request = sqlConnection.getConnection().prepareStatement("SELECT * From `Player` WHERE UUID=?");
            request.setString(1, getUUID().toString());
            ResultSet result = request.executeQuery();
            if (result.next()){
            	
	            name = result.getString(3);
	            rank = result.getInt(4);
	            klass = result.getInt(5);
	            money = result.getInt(6);
	            bank = result.getInt(7);
	            home = LocationUtils.toLoc(result.getString(8));
	            logout = LocationUtils.toLoc(result.getString(9));
	            setLife(result.getInt(10));
	            setMana(result.getInt(11));
	            language = result.getString(12);
            }
            request.close();
            result.close();
            
        } catch (SQLException e) {
        	
        	Bukkit.getLogger().info("SQL select Failed" + e.toString());
        }
        
        if(klass != Klassen.NONE){
        	
	        try {
	            PreparedStatement request = sqlConnection.getConnection().prepareStatement("SELECT * From `Klassen` WHERE UUID=? AND Class=?");
	            request.setString(1, getUUID().toString());
	            request.setInt(2, klass);
	            ResultSet result = request.executeQuery();
	            if (result.next()){
	            	
		            level = result.getInt(4);
		            xp = result.getInt(5);
	            }
	            request.close();
	            result.close();
	            
	        } catch (SQLException e) {
	        	
	        	Bukkit.getLogger().info("SQL select Failed" + e.toString());
	        }
        }
        
        Player player = Bukkit.getPlayer(getUUID());
        	
        if(player != null){
        	
        	player.teleport(getLogout());
        }
        
        update();
	} 
	
	public void save(){
        
		Player player = Bukkit.getPlayer(getUUID());
		saveStats(player);
		saveKlasse();
		update();
	}

	public void saveStats(Player player){
		
		Tenyrian plugin = Tenyrian.getInstance();
		SqlConnection sqlConnection = Tenyrian.getInstance().getSqlConnection();
		
        try {
        	PreparedStatement update = sqlConnection.getConnection().prepareStatement("UPDATE `Player` SET PlayerName=?, Rank=?, Class=?, Money=?, MoneyBank=?, HomePosition=?, LogoutPosition=?, Life=?, Mana=?, Language=? WHERE uuid=?;");
            update.setString(1, name);
            update.setInt(2, rank);
            update.setInt(3, klass);
            update.setInt(4, money);
            update.setInt(5, bank);
            update.setString(6, LocationUtils.toString(home));
            if(player != null){
            	
            	update.setString(7, LocationUtils.toString(player.getLocation()));
            }else{
            	
            	update.setString(7, LocationUtils.toString(logout));
            }
            update.setInt(8, life);
            update.setInt(9, mana);
            update.setString(10, language);
            update.setString(11, getUUID().toString());
            update.executeUpdate();
            update.close();
            
        } catch (SQLException e) {
            Bukkit.getLogger().info("SQL update Failed" + e.toString());
        }
	}
	
	public void saveKlasse(){
	
		Tenyrian plugin = Tenyrian.getInstance();
		SqlConnection sqlConnection = Tenyrian.getInstance().getSqlConnection();
		
        if(klass != Klassen.NONE){
        	
	        try {
	        	PreparedStatement update = sqlConnection.getConnection().prepareStatement("UPDATE `Klassen` SET Level=?, Xp=? WHERE UUID=? AND Class=?;");
	            update.setInt(1, level);
	            update.setInt(2, xp);
	
	            update.setString(3, getUUID().toString());
	            update.setInt(4, klass);
	            update.executeUpdate();
	            update.close();
	            
	        } catch (SQLException e) {
	            Bukkit.getLogger().info("SQL update Failed" + e.toString());
	        }
        }
	}
	
	public void doTick(Player player){
		
	}
	
	public void update(){
	
		Player player = Bukkit.getPlayer(getUUID());
		
		if(player != null){
			
	        ScoreboardManager manager = Bukkit.getScoreboardManager();
		    Scoreboard board = manager.getNewScoreboard();	    	    
		    Objective objective = board.registerNewObjective("anzeige", "dummy");
		    objective.setDisplayName("§l§7§l"+name);
		    objective.setDisplaySlot(DisplaySlot.SIDEBAR);
	
		    Score score = objective.getScore("§l§c§l" + Chat.getString(language, Chat.LifeName) + "§8: "); 
			score.setScore(10);
			
		    score = objective.getScore(life + "/" + Klassen.getMaxLife(klass, level)); 
			score.setScore(9);
			
			if(klass != Klassen.NONE){
				
			    score = objective.getScore(""); 
				score.setScore(8);	 
				
			    score = objective.getScore("§l§9§l" + Klassen.getManaName(language, klass) + "§8: "); 
				score.setScore(7);
			
			    score = objective.getScore(mana + "/" + Klassen.getMaxMana(klass, level)); 
				score.setScore(6);
				
			    score = objective.getScore(" "); 
				score.setScore(5);	
				
			    score = objective.getScore("§l§e§l" + Chat.getString(language, Chat.LevelName) + "§8:"); 
			    score.setScore(4);
		   		int maxXp = Klassen.getMaxXP(level);
	    		int prozentXp = (int)(xp/((float)maxXp/100));
	    		
	    	    score = objective.getScore("(§e" + level + "§f) " + prozentXp + "%"); 
	    	    score.setScore(3);
			}
			
			player.setScoreboard(board);
		}
	}
	
	public void changeKlass(int klasse){
		if(klass != klasse){
			
			saveKlasse();
			
			klass = klasse;
			saveStats(PlayerUtils.getPlayer(this));
			
			if(!existKlass(klass)){
				
				createKlass(klass);
			}
			
			load();
			
			setFullLife();
			setFullMana();
			update();
		}
	}
	
	public UUID getUUID(){
		
		return uuid;
	}
	
	public Location getLogout(){
		return logout;
	}
	
	public int getRank(){
		return rank;
	}
	
	public String getLanguage(){
		return language;
	}

	public int getKlass() {
		return klass;
	}

	public void setKlass(int klass) {
		this.klass = klass;
		
		update();
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
		
		update();
	}

	public int getBank() {
		return bank;
	}

	public void setBank(int bank) {
		this.bank = bank;
		
		update();
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		
		if(klass != Klassen.NONE){
			
			this.level = level;
			
			if(life > Klassen.getMaxLife(klass, level)) setFullLife();
			if(mana > Klassen.getMaxMana(klass, level)) setFullMana();
			
			setXP(0);
			
			TablistUtils.update();
			
			if(level == Klassen.MaxLevel){
				
				TitleUtils.create(uuid, "§6Levelup", "§c" + "Maxlevel");
			}else{
				
				TitleUtils.create(uuid, "§6Levelup", "§e" + level);
			}
	
			Player player = Bukkit.getPlayer(uuid);
			
			if(player != null){
				
				//PartikelUtils.create(EnumParticle.FLAME, player.getLocation().toVector(), new Vector(1, 1, 1), 0, 100);
				PartikelUtils.createFireHelix(player, 80);
				player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 0);
			}
		}
	}
	
	public void addLevel(int level) {

		setLevel(this.level + level);
	}

	public int getXP() {
		return xp;
	}

	public void setXP(int xp) {

		if(klass != Klassen.NONE){
			
			this.xp = xp;
		}
		update();
	}
	
	public void addXP(int xp) {
		
		if(level < Klassen.MaxLevel){
			
			if(this.xp + xp < Klassen.getMaxXP(level)){
				
				this.xp += xp;
				update();
			}else{
				
				int i = this.xp + xp;
				while(i > Klassen.getMaxXP(level)){
					
					i -= Klassen.getMaxXP(level);
					level ++;
				}
				
				if(level > Klassen.MaxLevel){
					
					setLevel(Klassen.MaxLevel);
				}else{
					
					setLevel(level);
				}
				
				if(level < Klassen.MaxLevel) setXP(i);
			}
			
		}else{
			
			level = 100;
			xp = 0;
		}
	}

	public int getLife() {
		return life;
	}

	public void setLife(int life) {
		
		this.life = life;
		
		int maxLife = Klassen.getMaxLife(klass, level);
		if(life > maxLife) this.life = maxLife;
		
		Player player = PlayerUtils.getPlayer(this);
		int realHealth = (int) player.getHealth();
		int realNewHealth = (this.life/(maxLife/20));
		
		if(this.life > 1 && realNewHealth <= 0) realNewHealth = 1;
		player.setHealth(realNewHealth);
		
		update();
	}
	
	public void addLife(int life) {
		setLife(getLife()+life);
	}
	
	public void setDamage(int life) {
		setLife(getLife()-life);
	}
	
	public void setFullLife() {
		
		setLife(Klassen.getMaxLife(klass, level));
		update();
	}

	public int getMana() {
		return mana;
	}

	public void setMana(int mana) {
		this.mana = mana;
		
		int maxMana = Klassen.getMaxMana(klass, level);
		if(this.mana > maxMana) this.mana = maxMana;
		if(this.mana < 0) this.mana = 0;
		update();
	}
	
	public void setFullMana() {
		
		setMana(Klassen.getMaxMana(klass, level));
		update();
	}

	public Location getHome() {
		return home;
	}

	public void setHome(Location home) {
		this.home = home;
		
		update();
	}

	public UUID getUuid() {
		return uuid;
	}

	public String getName() {
		return name;
	}

	public void setRank(int rank) {
		this.rank = rank;
		
		TablistUtils.update();
		update();
	}

	public void setLogout(Location logout) {
		this.logout = logout;
	}

	public void setLanguage(String language) {
		this.language = language;
		
		update();
	}
}
