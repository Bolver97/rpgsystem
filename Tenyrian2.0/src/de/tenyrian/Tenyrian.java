package de.tenyrian;

import org.bukkit.plugin.java.JavaPlugin;

import de.tenyrian.chat.ChatManager;
import de.tenyrian.command.CommandManager;
import de.tenyrian.config.DefaultConfig;
import de.tenyrian.folder.DefaultFolder;
import de.tenyrian.klasse.KlassenManager;
import de.tenyrian.language.LanguageManager;
import de.tenyrian.menue.MenueManager;
import de.tenyrian.monster.MonsterManager;
import de.tenyrian.region.RegionManager;
import de.tenyrian.rpgplayer.RpgPlayerManager;
import de.tenyrian.runtask.RunTaskManager;
import de.tenyrian.sql.SqlConnection;
import de.tenyrian.tablist.Tablist;

public class Tenyrian extends JavaPlugin{

	private SqlConnection sqlConnection;
	private LanguageManager languageManager;
	private MonsterManager monsterManager;
	private MenueManager menueManager;
	private RegionManager regionManager;
	private KlassenManager klassenManager;
	private RpgPlayerManager rpgPlayerManager;
	private Tablist tablist;
	private CommandManager commandManager;
	
	private static Tenyrian instance;
	
	public void onEnable(){
		
		instance = this;
		
		new DefaultFolder(this);
		new DefaultConfig(this); 
		
		sqlConnection = new SqlConnection();
		languageManager = new LanguageManager(this);
		monsterManager = new MonsterManager(this);
		menueManager = new MenueManager(this);
		regionManager = new RegionManager(this);
		klassenManager = new KlassenManager(this);
		commandManager = new CommandManager(this);
		languageManager.reload(); //reload Textfiles
		
		rpgPlayerManager = new RpgPlayerManager(this);
		tablist = new Tablist(this);
		
		new RunTaskManager(this);
		
	}
	
	public void onDisable(){
		
		rpgPlayerManager.save();
	}
	
	public SqlConnection getSqlConnection(){
		
		return sqlConnection;
	}
	
	public RpgPlayerManager getRpgPlayerManager(){
		
		return rpgPlayerManager;
	}
	
	public MenueManager getMenueManager(){
		
		return menueManager;
	}
	
	public LanguageManager getLanguageManager(){
		
		return languageManager;
	}
	
	public RegionManager getRegionManager(){
		
		return regionManager;
	}
	
	public Tablist getTablist(){
		
		return tablist;
	}

	public CommandManager getCommandManager(){
		
		return commandManager;
	}
	
	public ChatManager getChatManager(){
		
		return getCommandManager().getChatManager();
	}
	
	public static Tenyrian getInstance(){
		
		return instance;
	}
}
