package de.tenyrian.language;

import java.util.UUID;

import org.bukkit.entity.Player;

import de.tenyrian.Tenyrian;
import de.tenyrian.rpgplayer.RpgPlayer;
import de.tenyrian.rpgplayer.RpgPlayerManager;
import de.tenyrian.utils.TenyrianUtils;

public interface Chat {

	int PREFIX = 3;
	int TAB = 2;
	int DEFAULT = 1;
	int NONE = 0;
	
	public static void sendMessage(Player player, int type, String prefix, String key, String[] args){
		
		Tenyrian plugin = Tenyrian.getInstance();
		LanguageManager languageManager = plugin.getLanguageManager();
		RpgPlayer rpgPlayer = plugin.getRpgPlayerManager().getRpgPlayer(player);
		
		String language = rpgPlayer.getLanguage();
		String msg = languageManager.getString(language, key);
		
		if(args != null){
			
			for(int i = 0; i < args.length; i++){
				
				String[] split = args[i].split("=");
				msg = msg.replace(split[0], output(split[1]));
			}
		}
		
		switch (type) {
		
		case PREFIX:
			
			player.sendMessage("§8[§6" + prefix + "§8]§7 " + msg);
			break;
			
		case TAB:
			String out = "";
			for(int i = 0; i < prefix.length() + 3; i++) out = out + " ";
			player.sendMessage(out + "§7" + msg);
			break;
			
		case DEFAULT:

			player.sendMessage("§7" + msg);
			break;
			
		default:

			player.sendMessage(msg.replace("§7", "").replace("§8", "").replace("§6", ""));
			break;
		}
	}
	
	public static String getMessage(Player player, String key, String[] args){
		
		String msg = getString(player, key);
		
		if(args != null){
			
			for(int i = 0; i < args.length; i++){
				
				String[] split = args[i].split("=");
				msg = msg.replace(split[0], output(split[1]));
			}
		}
		
		return msg;
	}
	
	public static void sendMenue(Player player, String name, int size){
		
		String preSuf = "§7";
		for(int i = 0; i < size; i++){
			
			preSuf = preSuf + "=";
		}
		
		String out = preSuf + "§8[§6" + name + "§8]" + preSuf;
		player.sendMessage(out);
	}
	
	public static String getMessage(Player player, String key, String[] args, String msgColor, String argColor){
		
		String msg = getString(player, key);
		
		if(args != null){
			
			for(int i = 0; i < args.length; i++){
				
				String[] split = args[i].split("=");
				msg = msg.replace(split[0], output(split[1], argColor, msgColor));
			}
		}
		
		return msgColor + msg;
	}
	
	public static String output(String arg){

		return "§6" + arg + "§7";
	}
	
	public static String output(String arg, String prefix, String suffix){

		return prefix + arg + suffix;
	}
	
	public static String getString(String language, String key){
		
		Tenyrian plugin = Tenyrian.getInstance();
		LanguageManager languageManager = plugin.getLanguageManager();
		return languageManager.getString(language, key);
	}
	
	public static String getString(Player player, String key){
		
		RpgPlayerManager rpgPlayerManager = TenyrianUtils.getRpgPlayerManager();
		LanguageManager languageManager = TenyrianUtils.getLanguageManager();
		return languageManager.getString(rpgPlayerManager.getRpgPlayer(player).getLanguage(), key);
	}
	
	String NoPermission = "NoPermission";
	String CommandUsage = "CommandUsage";
	String PlayerIsOffline = "PlayerIsOffline";
	String PlayerDoesentExist = "PlayerDoesentExist";
	
	String LevelName = "stats.level";
	String LifeName = "stats.life";
	String ManaNameRouge = "stats.mana.rouge";
	String ManaNameBarbar = "stats.mana.barbar";
	String ManaNameKnight = "stats.mana.knight";
	String ManaNameWitch = "stats.mana.witch";
	String ManaNameHunter = "stats.mana.hunter";
	
	String KlassenNameRouge = "stats.name.rouge";
	String KlassenNameBarbar = "stats.name.barbar";
	String KlassenNameKnight = "stats.name.knight";
	String KlassenNameWitch = "stats.name.witch";
	String KlassenNameHunter = "stats.name.hunter";
}
