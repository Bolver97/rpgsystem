package de.tenyrian.language;

import java.io.File;
import java.util.ArrayList;

import de.tenyrian.Tenyrian;

public class LanguageManager {

	private ArrayList<Language> languages;
	private LanguageDefault languageDefault;
	
	private Tenyrian plugin;
	
	public LanguageManager(Tenyrian plugin){
		
		this.plugin = plugin;
		
		languageDefault = new LanguageDefault(plugin);
		
		load();
	}
	
	public void reload(){
		
		languageDefault.save();
		load();
	}
	
	public void load(){
		
		languages = new ArrayList<>();
		
		File[] files = new File(plugin.getDataFolder(), "language").listFiles();
		
		if(files != null){

			for (File file : files) {
				
			    if (file.isFile()) {
			        
			    	String name = file.getName().replace(".yml", "");
			    	
			    	languages.add(new Language(plugin, name));
			    }
			}
		}
	}
	
	public String getString(String languageName, String key){
		
		for(Language language : languages){
			
			if(language.getLanguageName().equals(languageName)){
				
				return language.getString("language." + key);
			}
		}
		
		return "[NULL]";
	}
	
	public void addDefaultString(String key, String name){
		
		languageDefault.addString(key, name);
	}
	
	public ArrayList<Language> getLanguages(){
		
		return languages;
	}
}
