package de.tenyrian.language;

import de.tenyrian.Tenyrian;
import de.tenyrian.config.EConfig;

public class LanguageDefault {

	private EConfig eConfig;
	private String name = "DE";
	
	private Tenyrian plugin;
	
	public LanguageDefault(Tenyrian plugin) {

		this.plugin = plugin;
		
		eConfig = new EConfig(plugin, "language/" + name + ".yml");
		
		load();
		
		eConfig.save();
	}
	
	public void load(){
		
		addString("Languagename", "Deutsch");
		addString("NoPermission", "Dafür besitzt du keine Berechtigung!");
		addString("CommandUsage", "Benutze @cmd");
	}
	
	public void addString(String key, String name){
		
		if(!eConfig.getYml().isSet("language." + key)){
			
			eConfig.getYml().set("language." + key, name);
		}
	}
	
	public void save(){
		
		eConfig.save();
	}
}
