package de.tenyrian.language;

import de.tenyrian.Tenyrian;
import de.tenyrian.config.EConfig;

public class Language {

	private EConfig eConfig;
	private String name;
	
	private Tenyrian plugin;
	
	public Language(Tenyrian plugin, String name) {

		this.plugin = plugin;
		this.name = name;
		
		eConfig = new EConfig(plugin, "language/" + name + ".yml");
	}
	
	public String getString(String path){
		
		return eConfig.getYml().getString(path, "[NULL]");
	}
	
	public String getLanguageName(){
		return name;
	}
	
	public String getRealLanguageName(){
		return getString("language.Languagename");
	}
}
