package de.tenyrian.chat;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.bukkit.Bukkit;

import de.tenyrian.sql.SqlConnection;
import de.tenyrian.utils.TenyrianUtils;

public class ChatFilter {
	
	private ArrayList<String> blockedWords;
	private ArrayList<String> blockedAddressList;
	
	public ChatFilter(){
		
		blockedAddressList = new ArrayList<>();
		blockedAddressList.add("de");
		blockedAddressList.add("com");
		blockedAddressList.add("net");
		blockedAddressList.add("eu");
		blockedAddressList.add("me");
		blockedAddressList.add("org");
		blockedAddressList.add("at");
		
		
		blockedWords = new ArrayList<>();
		
		SqlConnection sqlConnection = TenyrianUtils.getSqlConnection();
		
        try {
            PreparedStatement request = sqlConnection.getConnection().prepareStatement("SELECT * From `BlockedWords`");
            ResultSet result = request.executeQuery();
            while(result.next()){
            	
            	blockedWords.add(result.getString(2));
            }
            request.close();
            result.close();
            
        } catch (SQLException e) {
        	
        	Bukkit.getLogger().info("SQL select Failed" + e.toString());
        }
	}
	
	public String cleanMessage(String message){
		
		for(String word : blockedWords){
			
			if(message.toLowerCase().contains(" " + word + " ")){
				
				message = message.toLowerCase().replace(word, getMask(word));
				
			}else if(message.toLowerCase().startsWith(word + " ")){
				
				message = message.toLowerCase().replace(word, getMask(word));
				
			}else if(message.toLowerCase().contains(word + "!")){
				
				message = message.toLowerCase().replace(word, getMask(word));
				
			}else if(message.toLowerCase().contains(word + ".")){
				
				message = message.toLowerCase().replace(word, getMask(word));
				
			}else if(message.toLowerCase().contains(word + "?")){
				
				message = message.toLowerCase().replace(word, getMask(word));
				
			}else if(message.toLowerCase().endsWith(" " + word)){
				
				message = message.toLowerCase().replace(word, getMask(word));
			}else if(message.toLowerCase().endsWith(word) && message.toLowerCase().startsWith(word)){
				
				message = message.toLowerCase().replace(word, getMask(word));
			}
		}
		
		return message;
	}
	
	private String getMask(String word){
		
		String mask = "";
		for(int i = 0; i < word.length(); i++) mask = mask + "*";
		return mask;
	}
	
	public boolean containsAddress(String message){
		
		for(String address : blockedAddressList){
			
			if(message.toLowerCase().contains("." + address)){
				return true;
			}else if(message.toLowerCase().replace(" ", "").contains("." + address)){
				return true;
			}else if(message.toLowerCase().contains(" " + address + " ")){
				return true;
			}else if(message.toLowerCase().endsWith(address)){
				return true;
			}else if(message.toLowerCase().contains(address + "!")){
				return true;
			}
		}
		return false;
	}
	
	public boolean existWord(String word){
		
		SqlConnection sqlConnection = TenyrianUtils.getSqlConnection();
		
        try {
            PreparedStatement request = sqlConnection.getConnection().prepareStatement("SELECT * From `BlockedWords` WHERE Word=?");
            request.setString(1, word.toLowerCase());
            ResultSet result = request.executeQuery();
            if (!result.next()) {
            	
                return false;
            }else{
            	
            	return true;
            }

        } catch (SQLException e) {
        	
        	return false;
        }
	}
	
	public void addWord(String word){
		
		blockedWords.add(word.toLowerCase());
		
		SqlConnection sqlConnection = TenyrianUtils.getSqlConnection();
		
        try {
        	
        	PreparedStatement insert = sqlConnection.getConnection().prepareStatement("INSERT INTO `BlockedWords` values(0,?);");
            insert.setString(1, word.toLowerCase());// word
            insert.executeUpdate();
            insert.close();
        } catch (SQLException e) {
        	
             Bukkit.getLogger().info("SQL insert" + e.toString());
        }
	}
	
	public void removeWord(String word){
		
		blockedWords.remove(word.toLowerCase());
		
		SqlConnection sqlConnection = TenyrianUtils.getSqlConnection();
		
        try {
        	PreparedStatement update = sqlConnection.getConnection().prepareStatement("DELETE FROM `BlockedWords` WHERE Word=?;");
            update.setString(1, word.toLowerCase());
            update.executeUpdate();
            update.close();
            
        } catch (SQLException e) {
            Bukkit.getLogger().info("SQL remove Failed" + e.toString());
        }
	}
	
	public ArrayList<String> getWords(){
		
		return blockedWords;
	}
}
