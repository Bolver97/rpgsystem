package de.tenyrian.chat;

import java.util.ArrayList;

public class CommandWhitelist {

	ArrayList<String> cmdWhitelist = new ArrayList<>();
	
	public CommandWhitelist(){
		
		addCommand("/reload");
	}
	
	public boolean isCommandWhitelistet(String cmd){
		return cmdWhitelist.contains(cmd);
	}
	
	public void addCommand(String cmd){
		
		cmdWhitelist.add(cmd);
	}
}
