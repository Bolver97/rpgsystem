package de.tenyrian.chat;

import org.bukkit.entity.Player;

public abstract class CommandListener {

	public abstract boolean onCommand(Player player, String cmd, String[] args);
}
