package de.tenyrian.chat;

public interface ChatType {

	public int Global = 0;
	public int Local = 1;
	public int Guild = 2;
}
