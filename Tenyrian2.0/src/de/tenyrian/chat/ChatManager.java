package de.tenyrian.chat;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

import de.tenyrian.Tenyrian;
import de.tenyrian.language.Chat;
import de.tenyrian.language.LanguageManager;
import de.tenyrian.rank.Rank;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class ChatManager implements Listener{

	private HashMap<Player, Integer> chatList = new HashMap<>();
	private HashMap<String, CommandListener> commands = new HashMap<>();
	
	private int lokalRange;
	
	private CommandWhitelist whitelist;
	private ChatFilter chatFilter;
	private Tenyrian plugin;
	
	public ChatManager(Tenyrian plugin){
		
		this.plugin = plugin;
		
		whitelist = new CommandWhitelist();
		chatFilter = new ChatFilter();
		
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		
		lokalRange = plugin.getConfig().getInt("config.chat.local.range", 50);
		
		LanguageManager lm = plugin.getLanguageManager();
		lm.addDefaultString("chat.contains.address", "Serverwerbung ist verboten und wird hart bestraft!");
		
		for(Player player : Bukkit.getServer().getOnlinePlayers()){
			
			setPlayerChat(player, ChatType.Global);
		}
	}
	
	public void registerCommand(String command, CommandListener commandListener){
		
		commands.put(command.toLowerCase(), commandListener);
	}
	
	public void registerCommand(String command, String commandShort, CommandListener commandListener){
		
		commands.put(command.toLowerCase(), commandListener);
		commands.put(commandShort.toLowerCase(), commandListener);
	}
	
	//Commands
    @EventHandler (priority = EventPriority.HIGHEST)
    public void onPlayerCommand(PlayerCommandPreprocessEvent event)
    {
    	
    	Player player = event.getPlayer();
    	String[] args = event.getMessage().split(" ");
    	
    	if(commands.containsKey(args[0].toLowerCase())){
    		
    		String string = event.getMessage();
    		String[] outputArgs;
    		if(args.length > 1){
    			
    			string = event.getMessage().substring(args[0].length() + 1);
    			outputArgs = string.split(" ");
    		}else{
    			
    			string = null;
    			outputArgs = new String[0];
    		}
    		
        	CommandListener commandListener = commands.get(args[0].toLowerCase());
        	
        	if(!commandListener.onCommand(player, args[0].substring(1), outputArgs)){
        		
        		player.sendMessage(args[0]);
        	}
        	
        	event.setCancelled(true);
    	}else{
    		
    		if(!whitelist.isCommandWhitelistet(args[0].toLowerCase())) event.setCancelled(true);
    	}
    }
    
    //UserChat
    @EventHandler (priority = EventPriority.HIGHEST)
    public void onPlayerChat(AsyncPlayerChatEvent event)
    {
    	Player player = event.getPlayer();
    	String message = chatFilter.cleanMessage(event.getMessage());
    	int chatType = getPlayerChat(player);
    	event.setCancelled(true);
    	
    	if(chatFilter.containsAddress(message)){
    		
    		Chat.sendMessage(player, Chat.PREFIX, "Chat", "chat.contains.address", null);
    		return;
    	}
    	
    	switch (chatType) {
		case ChatType.Global:
			
	    	for(Player p : Bukkit.getServer().getOnlinePlayers()){
	    		
	    		int chatTypeTarget = getPlayerChat(p);
	    		
	    		if(chatTypeTarget == chatType){
	    			
	    			TextComponent msg = new TextComponent("§8[§6global§8] ");
		    		msg.addExtra(Rank.getChatPrefix(player));
		    		msg.addExtra(": " + message);
		    		p.spigot().sendMessage(msg);
	    		}
	    	}
			break;
		case ChatType.Local:
			
	    	for(Player p : Bukkit.getServer().getOnlinePlayers()){
	    		
	    		if(p.getLocation().toVector().distance(player.getLocation().toVector()) <= lokalRange){
	    		
		    		int chatTypeTarget = getPlayerChat(p);
		    		
		    		if(chatTypeTarget == chatType){
		    			
		    			TextComponent msg = new TextComponent("§8[§6local§8] ");
			    		msg.addExtra(Rank.getChatPrefix(player));
			    		msg.addExtra(": " + message);
			    		p.spigot().sendMessage(msg);
		    		}
	    		}
	    	}
			break;
		default:
			break;
		}
    }
    
    
    @EventHandler (priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent event)
    {

    	Player player = event.getPlayer();
    	setPlayerChat(player, ChatType.Global);
    }
    
    @EventHandler (priority = EventPriority.HIGHEST)
    public void onPlayerLeave(PlayerQuitEvent event)
    {

    	Player player = event.getPlayer();
    	chatList.remove(player);
    }
    
    public void setPlayerChat(Player player, int chatType){
    	chatList.put(player, chatType);
    }
    
    public int getPlayerChat(Player player){
    	return chatList.get(player);
    }
    
    public ChatFilter getChatFilter(){
    	
    	return chatFilter;
    }
}
