package de.tenyrian.damage;

import java.util.List;

import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.metadata.MetadataValue;

import de.tenyrian.monster.MonsterStats;
import de.tenyrian.utils.LivingEntityUtils;

public interface Damage {

    public static void onPlayerByPlayer(EntityDamageEvent event)
    {

    	event.setCancelled(true);
    }
    
    public static void onEntityByPlayer(EntityDamageEvent event)
    {

    	LivingEntityUtils.updateHealthBar((LivingEntity) event.getEntity(), (int) event.getDamage());
    	//event.setCancelled(true);
    }
    
    public static void onOtherByPlayer(EntityDamageEvent event)
    {

    	switch (event.getCause()) {
		case FALL:
			
			break;

		default:
			break;
		}
    	event.setCancelled(true);
    }
    
    public static void onPlayerByEntity(EntityDamageEvent event)
    {

    	event.setCancelled(true);
    }
    
    public static void onEntityByEntity(EntityDamageEvent event)
    {
	    EntityDamageByEntityEvent edbeEvent = (EntityDamageByEntityEvent)event;
	    Entity damager = edbeEvent.getDamager();
    	Entity entity = event.getEntity();
    	if(damager.hasMetadata("TenyrianMonster")){
    		
    		List<MetadataValue> meta = damager.getMetadata("TenyrianMonster");
    		int damage = MonsterStats.getMonsterDamage(meta.get(0).asInt());
    		
    		event.setDamage(damage);
    		
    		LivingEntityUtils.updateHealthBar((LivingEntity) entity, (int) event.getFinalDamage());
    	}else{
    		
    		event.setCancelled(true);
    	}
    }
    
    public static void onOtherByEntity(EntityDamageEvent event)
    {

    	event.setCancelled(true);
    }
}
