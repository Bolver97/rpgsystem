package de.tenyrian.menue;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import de.tenyrian.rpgplayer.RpgPlayer;

public abstract class MenueListener {
	
	public abstract void onClick(Player player, int slot, ItemStack itemstack, int menueAction);
	public abstract Menue getMenue();
}
