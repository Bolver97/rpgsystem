package de.tenyrian.menue;

import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import de.tenyrian.Tenyrian;
import de.tenyrian.rpgplayer.RpgPlayer;
import de.tenyrian.utils.PlayerUtils;

public class MenueManager implements Listener {

	private HashMap<Player, Menue> menueOpen = new HashMap<>();
	private HashMap<Menue, MenueListener> menueMap = new HashMap<>();
	
	private Tenyrian plugin;
	
	public MenueManager(Tenyrian plugin){
		
		this.plugin = plugin;
		
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	public void disable(){
		
		for(Player player : menueOpen.keySet()) player.closeInventory();
	}
	
	public void add(Menue menue, MenueListener menueListener){
		
		menueMap.put(menue, menueListener);
	}
	
	public void open(Player player, Menue menue){
		
		player.closeInventory();
		menue.open(player);
		
		if(!menueOpen.containsKey(player)) menueOpen.remove(player);
		menueOpen.put(player, menue);
	}
	
	
    @EventHandler 
    public void onInventoryCloseEvent(InventoryCloseEvent event){
    	
    	Player player = (Player) event.getPlayer();
    	if(menueOpen.containsKey(player)){
    		
    		Menue menue = menueOpen.get(player);
    		if(menue.deleteOnClose()) menueMap.remove(menue);
    			
    		menueOpen.remove(player); 
    	}
    }
    
    @EventHandler 
    public void onPlayerLeave(PlayerQuitEvent event){
    	
    	Player player = (Player) event.getPlayer();
    	if(menueOpen.containsKey(player)){
    		
    		Menue menue = menueOpen.get(player);
    		if(menue.deleteOnClose()) menueMap.remove(menue);
    			
    		menueOpen.remove(player); 
    	}
    }
    
    @EventHandler 
    public void onInventoryClickEvent(InventoryClickEvent event)
    {
    	
    	Player player = (Player) event.getWhoClicked();
    	
        if(event.getInventory() == null) return;
        
        Inventory clickedInventory = event.getClickedInventory();
        
    	if(!menueOpen.containsKey(player)) return;
    		
        ItemStack itemstack = event.getCurrentItem();
        	
        event.setCancelled(true);
        player.updateInventory();
        	
        if(itemstack == null) return;
        if(itemstack.getType() == Material.AIR) return;
	    if(clickedInventory.getType() == InventoryType.PLAYER) return;    			
	    
	    int slot = event.getSlot();
	    
	    Menue menue = menueOpen.get(player);
	    MenueListener menueListener = menueMap.get(menue);
	    
	    switch(event.getAction()) {
		case PICKUP_ALL: //Linksklick
			
			menueListener.onClick(player, slot, itemstack, MenueAction.LEFT_CLICK);
			break;
		case PICKUP_HALF: //Rechtsklick
			
			menueListener.onClick(player, slot, itemstack, MenueAction.RIGHT_CLICK);
			break;
	    }       
    }
}
