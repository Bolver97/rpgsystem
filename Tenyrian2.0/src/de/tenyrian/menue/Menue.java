package de.tenyrian.menue;

import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Menue {

	private HashMap<Integer, ItemStack> itemStacks = new HashMap<>();
	private String title;
	private int size;
	private boolean deleteOnClose;
	
	public Menue(String title, int size, boolean deleteOnClose){
		
		this.title = title;
		this.size = size;
	}
	
	public void addItemstack(int slot, ItemStack itemStack){
		
		itemStacks.put(slot, itemStack);
	}
	
	public void addItemstack(int slot, Material material){
		
		ItemStack itemStack = new ItemStack(material);
		addItemstack(slot, itemStack);
	}
	
	public void addItemstack(int slot, Material material, int amount){
		
		ItemStack itemStack = new ItemStack(material, amount);
		addItemstack(slot, itemStack);
	}
	
	public void addItemstack(int slot, Material material, int amount, short durability){
		
		ItemStack itemStack = new ItemStack(material, amount);
		itemStack.setDurability(durability);
		addItemstack(slot, itemStack);
	}
	
	public void addItemstack(int slot, Material material, int amount, short durability, String displayName){
		
		ItemStack itemStack = new ItemStack(material, amount);
		itemStack.setDurability(durability);
		ItemMeta itemMeta = itemStack.getItemMeta();
		itemMeta.setDisplayName(displayName);
		itemStack.setItemMeta(itemMeta);
		addItemstack(slot, itemStack);
	}
	
	public void addItemstack(int slot, Material material, int amount, short durability, String displayName, List<String> lore){
		
		ItemStack itemStack = new ItemStack(material, amount);
		itemStack.setDurability(durability);
		ItemMeta itemMeta = itemStack.getItemMeta();
		itemMeta.setDisplayName(displayName);
		itemMeta.setLore(lore);
		itemStack.setItemMeta(itemMeta);
		addItemstack(slot, itemStack);
	}

	public void open(Player player){
		
		Inventory inventory = Bukkit.createInventory(null, size, title);
		
		for(int slot : itemStacks.keySet()){
			
			inventory.setItem(slot, itemStacks.get(slot));
		}
		
		player.openInventory(inventory);
	}
	
	public String getTitle() {
		return title;
	}
	
	public boolean deleteOnClose() {
		return deleteOnClose;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public HashMap<Integer, ItemStack> getItemStacks() {
		return itemStacks;
	}
}
