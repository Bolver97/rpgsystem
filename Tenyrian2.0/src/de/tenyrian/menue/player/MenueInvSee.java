package de.tenyrian.menue.player;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import de.tenyrian.klasse.Klassen;
import de.tenyrian.language.Chat;
import de.tenyrian.language.Language;
import de.tenyrian.language.LanguageManager;
import de.tenyrian.menue.Menue;
import de.tenyrian.menue.MenueListener;
import de.tenyrian.menue.MenueManager;
import de.tenyrian.rpgplayer.RpgPlayer;
import de.tenyrian.utils.PlayerUtils;
import de.tenyrian.utils.TenyrianUtils;

public class MenueInvSee extends MenueListener{

	public Menue menue;
	
	public MenueInvSee(Player target) {
		
		MenueManager menueManager = TenyrianUtils.getMenueManager();
		
		menue = new Menue(target.getName() + "'s inventory", 36, true);
		for(int i = 0; i < 36; i++){

			menue.addItemstack(i, target.getInventory().getItem(i));
		}
		
		menueManager.add(menue, this);
	}
	
	@Override
	public void onClick(Player player, int slot, ItemStack itemstack, int menueAction) {
		
	}

	@Override
	public Menue getMenue() {
		return menue;
	}

}
