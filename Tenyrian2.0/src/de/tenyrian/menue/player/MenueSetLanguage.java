package de.tenyrian.menue.player;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import de.tenyrian.klasse.Klassen;
import de.tenyrian.language.Chat;
import de.tenyrian.language.Language;
import de.tenyrian.language.LanguageManager;
import de.tenyrian.menue.Menue;
import de.tenyrian.menue.MenueListener;
import de.tenyrian.menue.MenueManager;
import de.tenyrian.rpgplayer.RpgPlayer;
import de.tenyrian.utils.PlayerUtils;
import de.tenyrian.utils.TenyrianUtils;

public class MenueSetLanguage extends MenueListener{

	public Menue menue;
	
	public MenueSetLanguage() {

		LanguageManager lm = TenyrianUtils.getLanguageManager();
		
		lm.addDefaultString("language.success", "Deine Sprache wurde auf @a geändert!");
		
		MenueManager menueManager = TenyrianUtils.getMenueManager();
		
		menue = new Menue("Language", 9, false);
		int i = 0;
		for(Language laanguage : lm.getLanguages()){
			
			menue.addItemstack(i, Material.BOOK, 1, (short) 0, laanguage.getRealLanguageName());
			i++;
		}
		menueManager.add(menue, this);
	}
	
	@Override
	public void onClick(Player player, int slot, ItemStack itemstack, int menueAction) {
		
		player.closeInventory();
		
		LanguageManager languageManager = TenyrianUtils.getLanguageManager();
		
		int i = 0;
		Language language = null;
		for(Language l : languageManager.getLanguages()){
			
			if(i == slot) language = l;
			i++;
		}
		
		RpgPlayer rpgPlayer = PlayerUtils.getRpgPlayer(player);
		rpgPlayer.setLanguage(language.getLanguageName());
		
		Chat.sendMessage(player, Chat.PREFIX, "Info", "language.success", new String[]{"@a=" + language.getRealLanguageName()});
	}

	@Override
	public Menue getMenue() {
		return menue;
	}

}
