package de.tenyrian.tablist;

import java.lang.reflect.Field;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import de.tenyrian.Tenyrian;
import de.tenyrian.rank.Rank;
import de.tenyrian.rpgplayer.RpgPlayer;
import net.minecraft.server.v1_10_R1.IChatBaseComponent;
import net.minecraft.server.v1_10_R1.PacketPlayOutPlayerListHeaderFooter;
import net.minecraft.server.v1_10_R1.PlayerConnection;

public class Tablist implements Listener {
	
	private Tenyrian plugin;
	
	public Tablist(Tenyrian plugin){
		
		this.plugin = plugin;
		
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		
    	update();
	}
 
    public void update(){
    	
    	update(null);
    }
	
    public void update(UUID quitPlayerUUID){
    	
    	int onlinePlayers = Bukkit.getServer().getOnlinePlayers().size();
    	if(quitPlayerUUID != null){
    		
    		onlinePlayers --;
    	}else{
    		
    		quitPlayerUUID = UUID.randomUUID();
    	}
    	
    	for(Player player : Bukkit.getServer().getOnlinePlayers()){
    		
    		if(!quitPlayerUUID.equals(player.getUniqueId())){
    			
    			Tenyrian plugin = Tenyrian.getInstance();
    			RpgPlayer rpgPlayer = plugin.getRpgPlayerManager().getRpgPlayer(player);
    			
    			String levelString = "";
    			if(rpgPlayer.getLevel() != 0) levelString = "§8[§6" + rpgPlayer.getLevel() + "§8] ";
    			
    			player.setPlayerListName(levelString + Rank.getRankColor(rpgPlayer.getRank()) + player.getName());
    			player.setDisplayName(levelString + Rank.getRankColor(rpgPlayer.getRank()) + player.getName());
    		
    			sendTabTitle(player, "§7==========§8[§eTenyrian§8.§ede§8]§7==========", "§7---§8[§e" + onlinePlayers + "§7/§e" + (Bukkit.getServer().getMaxPlayers()) + "§8]§7---");
    		}
    	}
    }
	
    public static void sendTabTitle(Player player, String header, String footer) {
        if (header == null) {
            header = "";
        }
        header = ChatColor.translateAlternateColorCodes('&', header);

        if (footer == null) {
            footer = "";
        }
        footer = ChatColor.translateAlternateColorCodes('&', footer);

        header = header.replaceAll("%player%", player.getName());
        footer = footer.replaceAll("%player%", player.getName());

        PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
        IChatBaseComponent tabTitle = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + header + "\"}");
        IChatBaseComponent tabFoot = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + footer + "\"}");
        PacketPlayOutPlayerListHeaderFooter headerPacket = new PacketPlayOutPlayerListHeaderFooter(tabTitle);
        try {
            Field field = headerPacket.getClass().getDeclaredField("b");
            field.setAccessible(true);
            field.set(headerPacket, tabFoot);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.sendPacket(headerPacket);
        }
    }
}
