package de.tenyrian.monster;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntitySpawnEvent;
import de.tenyrian.Tenyrian;
import de.tenyrian.monster.typ.MonsterAggroHase;
import de.tenyrian.monster.typ.MonsterCaveSpider;
import de.tenyrian.monster.typ.MonsterCreeper;
import de.tenyrian.monster.typ.MonsterSkeleton;
import de.tenyrian.monster.typ.MonsterSpider;
import de.tenyrian.monster.typ.MonsterZombie;
import de.tenyrian.utils.LivingEntityUtils;
import de.tenyrian.utils.PercentUtils;

public class MonsterManager implements Listener {

	private PercentUtils percentMonsterUtils;
	private PercentUtils percentAnimalUtils;
	
	private Tenyrian plugin;
	
	public MonsterManager(Tenyrian plugin){
		
		this.plugin = plugin;
		
		percentMonsterUtils = new PercentUtils();
		percentAnimalUtils = new PercentUtils();
		
		load();
		
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		
		for(World world : Bukkit.getServer().getWorlds()){
			
			for(Entity entity : world.getEntities()){
				
				if(entity instanceof LivingEntity){
					
					switch (entity.getType()) {
					case PLAYER: break;
					case COW: break;
					case SHEEP: break;
					case CHICKEN: break;
					case HORSE: break;
					case PIG: break;
					case SQUID: break;
					case SNOWMAN: break;
					default:
						entity.remove();
						break;
					}
					
					if(entity != null){
						
						LivingEntityUtils.updateHealthBar((LivingEntity) entity);
					}
				}
			}
		}
	}
	
	public void load(){
		
		percentMonsterUtils.add(new MonsterZombie(), 1000);
		percentMonsterUtils.add(new MonsterSkeleton(), 800);
		percentMonsterUtils.add(new MonsterCreeper(), 400);
		percentMonsterUtils.add(new MonsterSpider(), 400);
		percentMonsterUtils.add(new MonsterCaveSpider(), 200);
		percentMonsterUtils.add(new MonsterAggroHase(), 1);
	}
	
    @EventHandler 
    public void onEntitySpawnEvent(CreatureSpawnEvent event){
    	
    	if(event.getEntity() instanceof LivingEntity){
    		
    		if(event.getEntity() instanceof Player) return;
    		if(event.getSpawnReason() == SpawnReason.CUSTOM) return;
    		event.setCancelled(true);
    		
    		if(event.getLocation().getBlock().getLightLevel() <= 7){
	    		if(percentMonsterUtils.hasNext()){
	    			
		    		Monster monster = (Monster) percentMonsterUtils.getRandom();
		    		LivingEntity livingEntity = monster.spawn(event.getLocation());
		    		monster.update(livingEntity);
	    		}
    		}else{
    			
	    		if(percentAnimalUtils.hasNext()){
	    			
		    		Monster monster = (Monster) percentAnimalUtils.getRandom();
		    		LivingEntity livingEntity = monster.spawn(event.getLocation());
		    		monster.update(livingEntity);
	    		}
    		}
    	}
    }
}
