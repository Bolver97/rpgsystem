package de.tenyrian.monster;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.metadata.FixedMetadataValue;

import de.tenyrian.Tenyrian;
import de.tenyrian.utils.LivingEntityUtils;

public abstract class Monster {

	public abstract LivingEntity spawn(Location location);
	
	public void update(LivingEntity livingEntity){
		
		LivingEntityUtils.updateHealthBar(livingEntity);
	}
	
	public void setType(LivingEntity livingEntity, int monsterType){
		
		livingEntity.setMetadata("TenyrianMonster", new FixedMetadataValue(Tenyrian.getInstance(), monsterType));
	}
	
	public void setLife(LivingEntity livingEntity, int monsterType){
		
		setType(livingEntity, monsterType);
		livingEntity.setMaxHealth(MonsterStats.getMonsterLife(monsterType));
		livingEntity.setHealth(MonsterStats.getMonsterLife(monsterType));
	}
}
