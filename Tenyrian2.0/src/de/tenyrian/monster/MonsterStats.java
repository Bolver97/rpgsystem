package de.tenyrian.monster;

public interface MonsterStats {

	public static int getMonsterLife(int monsterType){
		
		switch (monsterType) {
		case MonsterType.Zombie: return 600;
		case MonsterType.Skeleton: return 500;
		case MonsterType.Creeper: return 400;
		case MonsterType.Blaze: return 500;
		case MonsterType.Ghast: return 200;
		case MonsterType.Spider: return 400;
		case MonsterType.Silverfish: return 900;
		case MonsterType.Guardian: return 1000;
		case MonsterType.Cavespider: return 800;
		case MonsterType.AggroHase: return 20000;
		case MonsterType.AggroWolf: return 1400;
		case MonsterType.ZombiePigman: return 700;
		case MonsterType.MagmaCube: return 400;
		case MonsterType.Slime: return 400;
		case MonsterType.Endermilbe: return 700;
		case MonsterType.Hexe: return 1100;
		case MonsterType.WitherSkelett: return 200;
		case MonsterType.Enderman: return 600;
		
		case MonsterType.BossZombie: return 8000;
		case MonsterType.BossCreeper: return 2000;
		case MonsterType.BossSkeleton: return 6500;
		case MonsterType.BossSpider: return 7000;
		case MonsterType.BossSlime: return 1400;
		default: return 600;
		}
	}
	
	public static int getMonsterDamage(int monsterType){
		
		switch (monsterType) {
		case MonsterType.Zombie: return 150;
		case MonsterType.Skeleton: return 150;
		case MonsterType.Creeper: return 400;
		case MonsterType.Blaze: return 75;
		case MonsterType.Ghast: return 200;
		case MonsterType.Spider: return 150;
		case MonsterType.Silverfish: return 250;
		case MonsterType.Guardian: return 75;
		case MonsterType.Cavespider: return 100;
		case MonsterType.AggroHase: return 500;
		case MonsterType.AggroWolf: return 200;
		case MonsterType.ZombiePigman: return 200;
		case MonsterType.MagmaCube: return 150;
		case MonsterType.Slime: return 150;
		case MonsterType.Endermilbe: return 250;
		case MonsterType.Hexe: return 250;
		case MonsterType.WitherSkelett: return 200;
		case MonsterType.Enderman: return 200;
		
		case MonsterType.BossZombie: return 275;
		case MonsterType.BossCreeper: return 1000;
		case MonsterType.BossSkeleton: return 250;
		case MonsterType.BossSpider: return 275;
		case MonsterType.BossSlime: return 350;
		default: return 0;
		}
	}
}
