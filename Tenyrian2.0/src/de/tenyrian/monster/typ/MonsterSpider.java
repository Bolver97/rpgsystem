package de.tenyrian.monster.typ;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Spider;

import de.tenyrian.monster.Monster;
import de.tenyrian.monster.MonsterType;
import de.tenyrian.utils.LivingEntityUtils;

public class MonsterSpider extends Monster{

	@Override
	public LivingEntity spawn(Location location) {

		Spider spider = (Spider) location.getWorld().spawnEntity(location, EntityType.SPIDER);
		LivingEntity livingEntity = (LivingEntity) spider;
		
		setLife(livingEntity, MonsterType.Spider);
		return livingEntity;
	}
}
