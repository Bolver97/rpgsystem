package de.tenyrian.monster.typ;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Zombie;
import de.tenyrian.monster.Monster;
import de.tenyrian.monster.MonsterType;
import de.tenyrian.utils.LivingEntityUtils;

public class MonsterZombie extends Monster{

	
	
	@Override
	public LivingEntity spawn(Location location) {
		
		Zombie zombie = (Zombie) location.getWorld().spawnEntity(location, EntityType.ZOMBIE);
		LivingEntity livingEntity = (LivingEntity) zombie;
		
		setLife(livingEntity, MonsterType.Zombie);
		return livingEntity;
	}

}
