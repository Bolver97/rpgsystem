package de.tenyrian.monster.typ;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Skeleton;
import de.tenyrian.monster.Monster;
import de.tenyrian.monster.MonsterType;
import de.tenyrian.utils.LivingEntityUtils;

public class MonsterSkeleton extends Monster{

	@Override
	public LivingEntity spawn(Location location) {

		Skeleton skeleton = (Skeleton) location.getWorld().spawnEntity(location, EntityType.SKELETON);
		LivingEntity livingEntity = (LivingEntity) skeleton;
		
		setLife(livingEntity, MonsterType.Skeleton);
		return livingEntity;
	}
}
