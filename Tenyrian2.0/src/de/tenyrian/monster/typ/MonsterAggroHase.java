package de.tenyrian.monster.typ;

import org.bukkit.Location;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Rabbit;
import org.bukkit.entity.Wolf;
import org.bukkit.entity.Rabbit.Type;

import de.tenyrian.monster.Monster;
import de.tenyrian.monster.MonsterType;

public class MonsterAggroHase extends Monster{

	@Override
	public LivingEntity spawn(Location location) {

		Rabbit rabbit = (Rabbit) location.getWorld().spawnEntity(location, EntityType.RABBIT);
		rabbit.setRabbitType(Type.THE_KILLER_BUNNY);
		LivingEntity livingEntity = (LivingEntity) rabbit;
		
		setLife(livingEntity, MonsterType.AggroHase);
		return livingEntity;
	}
}
