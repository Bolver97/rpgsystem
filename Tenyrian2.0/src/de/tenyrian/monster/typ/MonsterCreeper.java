package de.tenyrian.monster.typ;

import org.bukkit.Location;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import de.tenyrian.monster.Monster;
import de.tenyrian.monster.MonsterType;

public class MonsterCreeper extends Monster{

	@Override
	public LivingEntity spawn(Location location) {

		Creeper creeper = (Creeper) location.getWorld().spawnEntity(location, EntityType.CREEPER);
		LivingEntity livingEntity = (LivingEntity) creeper;
		
		setLife(livingEntity, MonsterType.Creeper);
		return livingEntity;
	}
}
