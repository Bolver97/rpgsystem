package de.tenyrian.monster.typ;

import org.bukkit.Location;
import org.bukkit.entity.CaveSpider;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import de.tenyrian.monster.Monster;
import de.tenyrian.monster.MonsterType;

public class MonsterCaveSpider extends Monster{

	@Override
	public LivingEntity spawn(Location location) {

		CaveSpider caveSpider = (CaveSpider) location.getWorld().spawnEntity(location, EntityType.CAVE_SPIDER);
		LivingEntity livingEntity = (LivingEntity) caveSpider;
		
		setLife(livingEntity, MonsterType.Creeper);
		return livingEntity;
	}
}
