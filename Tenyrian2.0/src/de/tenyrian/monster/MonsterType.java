package de.tenyrian.monster;

public interface MonsterType {

	public int Zombie = 0;
	public int Skeleton = 1;
	public int Creeper = 2;
	public int Blaze = 3;
	public int Ghast = 4;
	public int Spider = 5;
	public int Silverfish = 6;
	public int Guardian = 7;
	public int Cavespider = 8;
	public int AggroHase = 9;
	public int AggroWolf = 10;
	public int ZombiePigman = 11;
	public int MagmaCube = 12;
	public int Slime = 13;
	public int Endermilbe = 14;
	public int Hexe = 15;
	public int WitherSkelett = 16;
	public int Enderman = 17;
	public int BossZombie = 18;
	public int BossCreeper = 19;
	public int BossSkeleton = 20;
	public int BossSpider = 21;
	public int BossSlime = 22;
	public int BossSkeletonRider = 23;
}
