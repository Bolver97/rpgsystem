package de.tenyrian.klasse;

import java.util.ArrayList;

import de.tenyrian.Tenyrian;
import de.tenyrian.klasse.klassen.Barbar;
import de.tenyrian.klasse.klassen.Hunter;
import de.tenyrian.klasse.klassen.Knight;
import de.tenyrian.klasse.klassen.Rouge;
import de.tenyrian.klasse.klassen.Witch;
import de.tenyrian.language.Chat;
import de.tenyrian.language.LanguageManager;

public class KlassenManager {

	private ArrayList<Klasse> klassen = new ArrayList<>();
	
	private Tenyrian plugin;
	
	public KlassenManager(Tenyrian plugin){
		
		this.plugin = plugin;
		
		LanguageManager lm = plugin.getLanguageManager();
		lm.addDefaultString(Chat.ManaNameRouge, "Energie");
		lm.addDefaultString(Chat.ManaNameBarbar, "Wut");
		lm.addDefaultString(Chat.ManaNameKnight, "Glauben");
		lm.addDefaultString(Chat.ManaNameWitch, "Mana");
		lm.addDefaultString(Chat.ManaNameHunter, "Ausdauer");		
		
		lm.addDefaultString(Chat.KlassenNameRouge, "Schattenklinge");
		lm.addDefaultString(Chat.KlassenNameBarbar, "Barbar");
		lm.addDefaultString(Chat.KlassenNameKnight, "Tempelritter");
		lm.addDefaultString(Chat.KlassenNameWitch, "Hexer");
		lm.addDefaultString(Chat.KlassenNameHunter, "Waldläufer");	
		
		lm.addDefaultString(Chat.LifeName, "Leben");	
		lm.addDefaultString(Chat.LevelName, "Level");	
		
		load();
	}
	
	public void load(){
		
		klassen.add(new Barbar());
		klassen.add(new Hunter());
		klassen.add(new Knight());
		klassen.add(new Rouge());
		klassen.add(new Witch());
	}
}
