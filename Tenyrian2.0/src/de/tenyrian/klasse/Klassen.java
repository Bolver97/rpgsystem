package de.tenyrian.klasse;

import de.tenyrian.language.Chat;

public interface Klassen {

	int ROUGE = 5;
	int BARBAR = 4;
	int HUNTER = 3;
	int KNIGHT = 2;
	int WITCH = 1;
	int NONE = 0;
	
	int MaxLevel = 100;
	
	public static int getMaxXP(int level){
		
		return (int) (150+((1.17*level)*(1.17*level)));
	} 
	
	public static int getMaxLife(int klasse, int level){
		if(level > 0) level --;
		switch(klasse){
		
		case ROUGE:
			int startLife = 455;
			return startLife + (startLife/100)*level;
		
		case BARBAR:
			startLife = 550;
			return startLife + (startLife/100)*level;
			
		case HUNTER:
			startLife = 475;
			return startLife + (startLife/100)*level;
			
		case KNIGHT:
			startLife = 900;
			return startLife + (startLife/100)*level;
		
		case WITCH:
			startLife = 550;
			return startLife + (startLife/100)*level;
			
		case NONE:
			return 100;
		}
		
		return 100;
	} 
	
	public static int getMaxMana(int klasse, int level){
		if(level > 0) level --;
		switch(klasse){
		
		case ROUGE:
			
			int startMana = 100;
			return startMana + (startMana/100)*level;
		
		case BARBAR:
			startMana = 75;
			return startMana + (startMana/100)*level;
			
		case HUNTER:
			startMana = 100;
			return startMana + (startMana/100)*level;
			
		case KNIGHT:
			startMana = 62;
			return startMana + (startMana/100)*level;
		
		case WITCH:
			startMana = 125;
			return startMana + (startMana/100)*level;
			
		default:
			
			return 0;
		}
	} 
	
	public static String getManaName(String language, int klasse){
		
		switch(klasse){
		
		case ROUGE:
			return Chat.getString(language, Chat.ManaNameRouge);
		
		case BARBAR:
			return Chat.getString(language, Chat.ManaNameBarbar);
			
		case HUNTER:
			return Chat.getString(language, Chat.ManaNameHunter);
			
		case KNIGHT:
			return Chat.getString(language, Chat.ManaNameKnight);
		
		case WITCH:
			return Chat.getString(language, Chat.ManaNameWitch);
			
		default:
			return "NULL";
		}
	} 
	
	public static String getKlassenName(String language, int klasse){
		
		switch(klasse){
		
		case ROUGE:
			return Chat.getString(language, Chat.KlassenNameRouge);
		
		case BARBAR:
			return Chat.getString(language, Chat.KlassenNameBarbar);
			
		case HUNTER:
			return Chat.getString(language, Chat.KlassenNameHunter);
			
		case KNIGHT:
			return Chat.getString(language, Chat.KlassenNameKnight);
		
		case WITCH:
			return Chat.getString(language, Chat.KlassenNameWitch);
			
		default:
			return "NULL";
		}
	} 
}
