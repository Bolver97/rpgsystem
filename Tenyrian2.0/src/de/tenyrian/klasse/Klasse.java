package de.tenyrian.klasse;

import java.util.ArrayList;

import de.tenyrian.skill.Skill;

public abstract class Klasse {

	private ArrayList<Skill> skills = new ArrayList<>();
	
	public void addSkill(Skill skill){
		skills.add(skill);
	}
}
