package de.tenyrian.sql;

import de.tenyrian.Tenyrian;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

/**
 *
 * @author Jakob
 */
public class SqlConnection {

    private Connection connection;
    /*private static final String url = "jdbc:mysql://localhost:3306/minecraft";
     private static final String user = "root";
     private static final String password = ""; */

    private String url;
    private int port;
    private String user;
    private String password;

    public enum sqlOperations {

        SQL_Update, SQL_Insert, SQL_Select
    }

    public SqlConnection() {
        readSettings();
        openConnection();
        //createTables();
    }

    @Override
    protected void finalize() throws Throwable {
        closeConnection();
        super.finalize();
    }

    private void readSettings() {
    	
        FileConfiguration config = Tenyrian.getInstance().getConfig();
        url = config.getString("config.database.url");
        port = config.getInt("config.database.port");
        user = config.getString("config.database.user");
        password = config.getString("config.database.password");
    }
  

    private void openConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String complett = "jdbc:mysql://" + url + ":" + port + "/" + user;
            Bukkit.getLogger().info("[Tenyrian] Connecting to:" + complett);
            connection = DriverManager.getConnection(complett, user, password);
        } catch (Exception e) {
            Bukkit.getLogger().info("[Tenyrian] Error Connecting to SQL:" + e.toString());
        }
    }

    private void closeConnection() {
        try {
            connection.close();
        } catch (Exception e) {
            Bukkit.getLogger().info("[Tenyrian] Error Closing Connection");
        }
    }

    private void createTables() {
        try {
            PreparedStatement createPlayerData
                    = this.getConnection().prepareStatement("CREATE TABLE `player_data` ("
                            + "`uuid` varchar(36) NOT NULL," + "`name` varchar(17) NOT NULL," + "`level` int(11) DEFAULT NULL,"
                            + "`xp` int(11) DEFAULT NULL," + "`Gilde` varchar(17) NOT NULL," + "`kills` int(11) DEFAULT NULL,"
                            + "`deaths` int(11) DEFAULT NULL," + "`money` double DEFAULT NULL," + "`bank` double NOT NULL,"
                            + "`klasse` enum('NONE','BARBAR','HEXER','SCHATTENKLINGE','TEMPLER','WALDLÄUFER') NOT NULL,"
                            + "`home_x` int(11) NOT NULL," + "`home_y` int(11) NOT NULL," + "`home_z` int(11) NOT NULL,"
                            + "`arena_win` int(11) NOT NULL," + "`arena_lose` int(11) NOT NULL)");
            createPlayerData.execute();
            createPlayerData.close();
        } catch (Exception e) {}

    }

    public Connection getConnection() {
        try {
            if (connection != null && !connection.isClosed()) {
                return connection;
            }
            Bukkit.getLogger().info("[Tenyrian]Error Connection not open");
            if (connection == null) {
                Bukkit.getLogger().info("[Tenyrian] Null!");
            } else {
                Bukkit.getLogger().info("[Tenyrian] Closed!");
            }
        } catch (SQLException ex) {
        }
        openConnection();
        return connection;
    }

    public void insertNewUser(Player player) {
        PreparedStatement insert;
        try {                                                   //
            insert = this.getConnection().prepareStatement("INSERT INTO `Player` values(?,?,0,0,0,0,0,0,?,0,0,?,?,?,0,0,0,0,0,?,?);");
            insert.setString(1, player.getUniqueId().toString());// UUID
            insert.setString(2, player.getName());// Name
            insert.setString(3, "-");// Rank
            insert.setInt(4, 0);// Class
            insert.setInt(5, 50);// Money
            insert.setInt(6, 0);// Bank
            insert.executeUpdate();
            insert.close();
        } catch (SQLException e) {
             Bukkit.getLogger().info("SQL insert" + e.toString());
        }
    }

//    public void updateUser(Spieler s) {
//        PreparedStatement update;
//        try {
//            update = this.getConnection().prepareStatement("UPDATE `player_data` SET name=?, levelBarbar=?,  levelHexer=?, levelSchattenklinge=?, levelTempler=?, levelWaldläufer=?, xp=?,"
//                    + " gilde=?, kills=?, deaths=?, money=?, bank=?, klasse=?, home_x=?, home_y=?, home_z=?, arena_win=?, arena_lose=?, rank=?, title=? WHERE uuid=?;");
//            update.setString(1, s.name);
//            update.setInt(2, s.levelBarbar);
//            update.setInt(3, s.levelHexer);
//            update.setInt(4, s.levelSchattenklinge);
//            update.setInt(5, s.levelTempler);
//            update.setInt(6, s.levelWaldläufer);
//            update.setInt(7, s.xp);
//            if (methoden.getGuild(s.uuid) != null) {
//                update.setString(8, methoden.getGuild(s.uuid).gibName());
//            } else {
//                update.setString(8, "-");
//            }
//            update.setInt(9, s.kills);
//            update.setInt(10, s.deaths);
//            update.setDouble(11, s.money);
//            update.setDouble(12, s.bank);
//            update.setString(13, s.klasse.name());
//            if(s.home!=null)
//            {
//                update.setInt(14, (int) s.home.getX());
//                update.setInt(15, (int) s.home.getY());
//                update.setInt(16, (int) s.home.getZ());
//            }
//            else
//            {
//                update.setInt(14, (int) 0);
//                update.setInt(15, (int) 0);
//                update.setInt(16, (int) 0);
//            }  
//            update.setInt(17, s.win);
//            update.setInt(18, s.lose);
//            update.setString(19, s.rank);
//            update.setString(20, s.title);
//            
//            update.setString(21, s.uuid.toString());
//            update.executeUpdate();
//            update.close();
//        } catch (SQLException e) {
//            Bukkit.getLogger().info("SQL update Failed:" + e.toString());
//        }
//        saveSkills(s);
//    }
}
