package de.tenyrian.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import com.google.common.base.Joiner;

public interface LocationUtils {

	public static String toString(Location location){

		String[] loc = new String[6];
		loc[0] = location.getWorld().getName();
		loc[1] = String.valueOf(location.getX());
		loc[2] = String.valueOf(location.getY());
		loc[3] = String.valueOf(location.getZ());
		loc[4] = String.valueOf(location.getPitch());
		loc[5] = String.valueOf(location.getYaw());
		return Joiner.on(";").join(loc);
	}
	
	public static Location toLoc(String string){
		
		String[] loc = string.split(";");
		World world = Bukkit.getServer().getWorld(loc[0]);
		float x = Float.parseFloat(loc[1]);
		float y = Float.parseFloat(loc[2]);
		float z = Float.parseFloat(loc[3]);
		float pitch = Float.parseFloat(loc[4]);
		float yaw = Float.parseFloat(loc[5]);
		return new Location(world, x, y, z, yaw, pitch);
	}
}
