package de.tenyrian.utils;

import de.tenyrian.Tenyrian;
import de.tenyrian.tablist.Tablist;

public interface TablistUtils {

	public static void update(){
		
		Tablist tablist = Tenyrian.getInstance().getTablist();
		
		tablist.update();
	}
}
