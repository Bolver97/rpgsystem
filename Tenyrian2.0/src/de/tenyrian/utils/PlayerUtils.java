package de.tenyrian.utils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import de.tenyrian.chat.ChatManager;
import de.tenyrian.rpgplayer.RpgPlayer;
import de.tenyrian.rpgplayer.RpgPlayerManager;

public interface PlayerUtils {

	public static Player getPlayer(RpgPlayer rpgPlayer){
		
		return Bukkit.getServer().getPlayer(rpgPlayer.getUuid());
	}
	
	public static RpgPlayer getRpgPlayer(Player player){
		
		RpgPlayerManager rpgPlayerManager = TenyrianUtils.getRpgPlayerManager();
		return rpgPlayerManager.getRpgPlayer(player);
	}
	
	public static void addMoney(Player player, int money){
		RpgPlayer rpgPlayer = getRpgPlayer(player);
		int oldMoney = rpgPlayer.getMoney();
		rpgPlayer.setMoney(oldMoney + money);
	}
	
	public static void setMoney(Player player, int money){
		RpgPlayer rpgPlayer = getRpgPlayer(player);
		rpgPlayer.setMoney(money);
	}
	
	public static int getMoney(Player player){
		RpgPlayer rpgPlayer = getRpgPlayer(player);
		return rpgPlayer.getMoney();
	}
	
	public static void addLevel(Player player, int level){
		RpgPlayer rpgPlayer = getRpgPlayer(player);
		rpgPlayer.addLevel(level);
	}
	
	public static void setLevel(Player player, int level){
		RpgPlayer rpgPlayer = getRpgPlayer(player);
		rpgPlayer.setLevel(level);
	}
	
	public static int getLevel(Player player){
		RpgPlayer rpgPlayer = getRpgPlayer(player);
		return rpgPlayer.getLevel();
	}
	
	public static void addXP(Player player, int xp){
		RpgPlayer rpgPlayer = getRpgPlayer(player);
		rpgPlayer.addXP(xp);
	}
	
	public static void setXP(Player player, int xp){
		RpgPlayer rpgPlayer = getRpgPlayer(player);
		rpgPlayer.setXP(xp);
	}
	
	public static int getXP(Player player){
		RpgPlayer rpgPlayer = getRpgPlayer(player);
		return rpgPlayer.getXP();
	}
	
	public static void setChat(Player player, int chatType){
		ChatManager chatManager = TenyrianUtils.getChatManager();
		chatManager.setPlayerChat(player, chatType);
	}
	
	public static int getChat(Player player){
		ChatManager chatManager = TenyrianUtils.getChatManager();
		return chatManager.getPlayerChat(player);
	}
	
	public static void doTick(){
		
		for(Player player : Bukkit.getOnlinePlayers()){
			
			RpgPlayer rpgPlayer = getRpgPlayer(player);
			rpgPlayer.doTick(player);
		}
	}
}
