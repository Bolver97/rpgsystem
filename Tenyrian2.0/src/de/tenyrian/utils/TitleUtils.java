package de.tenyrian.utils;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public interface TitleUtils {

	public static void create(Player player, String title, String subtitle){
		
		player.sendTitle(title, subtitle);
	}
	
	public static void create(UUID uuid, String title, String subtitle){
		
		Player player = Bukkit.getPlayer(uuid);
		
		if(player != null) player.sendTitle(title, subtitle);
	}
}
