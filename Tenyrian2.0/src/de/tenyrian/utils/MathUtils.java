package de.tenyrian.utils;

import java.util.Random;

public interface MathUtils {

	public static boolean isInteger(String s) {
	    try { 
	        Integer.parseInt(s); 
	    } catch(NumberFormatException e) { 
	        return false; 
	    } catch(NullPointerException e) {
	        return false;
	    }
	    
	    return true;
	}
	
	public static int random(int min, int max) {

		return new Random().nextInt((max - min) + 1) + min;
	}
}
