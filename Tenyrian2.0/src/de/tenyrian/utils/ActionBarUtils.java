package de.tenyrian.utils;

import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_10_R1.IChatBaseComponent;
import net.minecraft.server.v1_10_R1.PacketPlayOutChat;
import net.minecraft.server.v1_10_R1.IChatBaseComponent.ChatSerializer;

public class ActionBarUtils {

	public static void create(Player player, String string){
		
		IChatBaseComponent barmsg = ChatSerializer.a("{\"text\":\"" + string + "\"}");
		PacketPlayOutChat bar = new PacketPlayOutChat(barmsg, (byte) 2);
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(bar);
	}
}
