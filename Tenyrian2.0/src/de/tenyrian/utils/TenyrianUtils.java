package de.tenyrian.utils;

import de.tenyrian.Tenyrian;
import de.tenyrian.chat.ChatFilter;
import de.tenyrian.chat.ChatManager;
import de.tenyrian.language.LanguageManager;
import de.tenyrian.menue.MenueManager;
import de.tenyrian.region.RegionManager;
import de.tenyrian.rpgplayer.RpgPlayerManager;
import de.tenyrian.sql.SqlConnection;

public interface TenyrianUtils {

	public static RpgPlayerManager getRpgPlayerManager(){
		
		Tenyrian tenyrian = Tenyrian.getInstance();
		return tenyrian.getRpgPlayerManager();
	}
	
	public static MenueManager getMenueManager(){
		
		Tenyrian tenyrian = Tenyrian.getInstance();
		return tenyrian.getMenueManager();
	}
	
	public static LanguageManager getLanguageManager(){
		
		Tenyrian tenyrian = Tenyrian.getInstance();
		return tenyrian.getLanguageManager();
	}
	
	public static RegionManager getRegionManager(){
		
		Tenyrian tenyrian = Tenyrian.getInstance();
		return tenyrian.getRegionManager();
	}
	
	public static ChatManager getChatManager(){
		
		Tenyrian tenyrian = Tenyrian.getInstance();
		return tenyrian.getChatManager();
	}
	
	public static ChatFilter getChatFilter(){
		
		return TenyrianUtils.getChatManager().getChatFilter();
	}
	
	public static SqlConnection getSqlConnection(){
		
		Tenyrian tenyrian = Tenyrian.getInstance();
		return tenyrian.getSqlConnection();
	}
}
