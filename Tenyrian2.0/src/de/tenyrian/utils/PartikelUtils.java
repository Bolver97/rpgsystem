package de.tenyrian.utils;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import de.tenyrian.Tenyrian;
import net.minecraft.server.v1_10_R1.EnumParticle;
import net.minecraft.server.v1_10_R1.PacketPlayOutWorldParticles;

public interface PartikelUtils {

	public static void create(EnumParticle partikel, Vector loc, Vector offset, float speed, int amount){
		
        PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(partikel, true, (float)loc.getX(), (float)loc.getY(), (float)loc.getZ(), (float)offset.getX(), (float)offset.getY(), (float)offset.getZ(), speed, amount, 0);
        for(Player online : Bukkit.getOnlinePlayers()) {
        	
            ((CraftPlayer)online).getHandle().playerConnection.sendPacket(packet);
        }
	}
	
	public static void createFireHelix(Entity entity, int ticksTime){
		
		Tenyrian plugin = Tenyrian.getInstance();

        new BukkitRunnable() {
            
            int ticks = 0;
            
            @Override
            public void run() {
            	
            	float posX = (float) (entity.getLocation().getX() + (Math.sin((float)ticks/5)));
            	float posZ = (float) (entity.getLocation().getZ() + (Math.cos((float)ticks/5)));
            	float posY = (float) entity.getLocation().getY() + (((float)ticks/(ticksTime))*2);
            	//float posY = (float) entity.getLocation().getY() + 2;
            	create(EnumParticle.VILLAGER_HAPPY, new Vector(posX, posY, posZ), new Vector(0, 0, 0), 0, 2);
            	create(EnumParticle.FLAME, entity.getLocation().toVector().add(new Vector(0, 1, 0)), new Vector(0.5f, 0.5f, 0.5f), 0, 2);
            	if(ticks >= ticksTime)cancel();
            	ticks++;
            }
        }.runTaskTimer(plugin, 0L, 1L);    
	}
}
