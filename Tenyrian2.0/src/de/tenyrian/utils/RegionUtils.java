package de.tenyrian.utils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import de.tenyrian.Tenyrian;
import de.tenyrian.region.Region;
import de.tenyrian.region.RegionManager;
import de.tenyrian.sql.SqlConnection;

public interface RegionUtils {

	public static boolean isColliding(UUID uuid, String name){
		
		RegionManager regionManager = Tenyrian.getInstance().getRegionManager();
		Player player = Bukkit.getServer().getPlayer(uuid);
		ArrayList<Region> regions  = regionManager.getCollisions(player);
		
		for(Region region : regions){
			
			if(region.getName().equalsIgnoreCase(name)) return true;
		}
		return false;
	}

	public static boolean isLocation(String name, Location location){
		
		RegionManager regionManager = Tenyrian.getInstance().getRegionManager();
		ArrayList<Region> regions  = regionManager.getCollisions(location);
		
		for(Region region : regions){
			
			if(region.getName().equalsIgnoreCase(name)) return true;
		}
		return false;
	}
	
	public static String getRegion(Player player){
		
		RegionManager regionManager = Tenyrian.getInstance().getRegionManager();
		ArrayList<Region> regions  = regionManager.getCollisions(player);
		
		String name = null;
		
		for(Region region : regions){
			
			if(name == null){
				
				name = region.getName();
			}else{
				
				switch(name){
				
				case "Spawn":
					if(!region.getName().equals("Battlezone")) name = region.getName();
					break;
				case "Battlezone":
					if(region.getName().equals("Spawn")) name = region.getName();
					break;
				default:
				
					name = region.getName();
					break;
				}
			}
		}
		
		return name;
	}
	
	public static void createSphere(String name, Location loc, int size){
		
		RegionManager regionManager = Tenyrian.getInstance().getRegionManager();
		
		if(exist(name)) regionManager.remove(name);
		
		regionManager.addSphere(name, loc, size);
	}
	
	public static void createChunk(String name, Location loc){
		
		RegionManager regionManager = Tenyrian.getInstance().getRegionManager();
		
		if(exist(name)) regionManager.remove(name);
		
		regionManager.addChunk(name, loc);
	}
	
	public static void createRegion(String name, Location loc01, Location loc02){
		
		RegionManager regionManager = Tenyrian.getInstance().getRegionManager();
		
		if(exist(name)) regionManager.remove(name);
		
		regionManager.addRegion(name, loc01, loc02);
	}
	
	public static void delete(String name){
		
		RegionManager regionManager = Tenyrian.getInstance().getRegionManager();
		regionManager.remove(name);
	}
	
	public static boolean exist(String name){
		
		Tenyrian plugin = Tenyrian.getInstance();
		SqlConnection sqlConnection = Tenyrian.getInstance().getSqlConnection();
		
        try {
            PreparedStatement request = sqlConnection.getConnection().prepareStatement("SELECT * From `Region` WHERE Name=?");
            request.setString(1, name);
            ResultSet result = request.executeQuery();
            if (!result.next()) {
            	
                return false;
            }else{
            	
            	return true;
            }

        } catch (SQLException e) {
        	
        	return false;
        }
	}
	
	public static void update(){
		
		RegionManager regionManager = TenyrianUtils.getRegionManager();
		
		for(Player player : Bukkit.getOnlinePlayers()){
			
			regionManager.update(player);
			
			String regionName = RegionUtils.getRegion(player);
			if(regionName == null){
				
				ActionBarUtils.create(player, "§8§l[§2§lWildnis§8§l]");
			}else{
				
				switch (regionName) {
				case "Spawn":
					
					ActionBarUtils.create(player, "§8§l[§6§l" + "Spawn" + "§8§l]");
					break;
					
				case "Battlezone":
					
					ActionBarUtils.create(player, "§8§l[§4§l" + "Kampfzone" + "§8§l]");
					break;

				default:
					
					ActionBarUtils.create(player, "§8§l[§e§l" + regionName + "§8§l]");
					break;
				}
			}
		}
	}
}
