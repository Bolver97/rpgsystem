package de.tenyrian.utils;

import org.bukkit.entity.LivingEntity;

public interface LivingEntityUtils {

	public static void updateHealthBar(LivingEntity livingEntity){
		updateHealthBar(livingEntity, 0);
	}
	
	public static void updateHealthBar(LivingEntity livingEntity, int damage){
		
		int maxHealth = (int) livingEntity.getMaxHealth();
		int health = (int) livingEntity.getHealth() - damage;
		if(health < 0) health = 0;
		float displayHealth = (float) (((float)health/maxHealth)*20);
		
		String livebar = "§8[§6" + health + "§8] ";
		
		for(int i = 0; i < displayHealth; i++) livebar = livebar + "§a|";
		for(int i = 0; i < 20 - displayHealth; i++) livebar = livebar + "§c|";
		livingEntity.setCustomNameVisible(true);
		livingEntity.setCustomName(livebar);
	}
}
