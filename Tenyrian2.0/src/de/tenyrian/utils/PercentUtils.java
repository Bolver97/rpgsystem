package de.tenyrian.utils;

import java.util.HashMap;

public class PercentUtils {

	public HashMap<Object, Integer> percentList;
	
	public PercentUtils(){
		
		percentList = new HashMap<>();
	}
	
	public void add(Object object, int percent){
		
		percentList.put(object, percent);
	}
	
	public Object getRandom(){
		
		int maxPercent = 0;
		int getPercent;
		
		for(Object obj : percentList.keySet()) maxPercent += percentList.get(obj);
		
		getPercent = MathUtils.random(0, maxPercent);
		
		int i = 0;
		for(Object obj : percentList.keySet()){
			
			i += percentList.get(obj);
			
			if(getPercent <= i) return obj;
		}
		
		return null;
	}
	
	public boolean hasNext(){
		return percentList.size() > 0 ? true : false;
	}
}
