package de.tenyrian.rank;

import org.bukkit.entity.Player;

import de.tenyrian.Tenyrian;
import de.tenyrian.rpgplayer.RpgPlayer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public interface Rank {

	int HEAD_ADMIN = 42;
	int ADMIN = 41;
	
	int MODERATOR = 32;
	int DEVELOPER = 31;
	
	int SUPPORTER = 23;
	int PR_MANAGER = 22;
	int KONZEPTLER = 21;
	
	int GRAFIKER = 13;
	int ARCHITEKT = 12;
	int PROBEMITGLIED = 11;
	
	int SPIELER_DIAMANT = 5;
	int SPIELER_GOLD = 4;
	int SPIELER_SILBER = 3;
	int SPIELER_BRONZE = 2;
	int SPIELER = 1;
	
	int GROUP_ADMIN = 40;
	int GROUP_MODERAOTR = 30;
	int GROUP_SUPPORTER = 20;
	int GROUP_SONSTIGE = 10;
	int GROUP_SPIELER_5 = 5;
	int GROUP_SPIELER_4 = 4;
	int GROUP_SPIELER_3 = 3;
	int GROUP_SPIELER_2 = 2;
	int GROUP_SPIELER_1 = 1;

	public static boolean hasPermissions(Player player, int group){
		
		Tenyrian plugin = Tenyrian.getInstance();
		RpgPlayer rpgPlayer = plugin.getRpgPlayerManager().getRpgPlayer(player);
		
		switch (group) {
		case GROUP_ADMIN:
			
			if(rpgPlayer.getRank() >= GROUP_ADMIN) return true;
			break;
		case GROUP_MODERAOTR:
			
			if(rpgPlayer.getRank() >= GROUP_MODERAOTR) return true;
			break;
		case GROUP_SUPPORTER:
			
			if(rpgPlayer.getRank() >= GROUP_SUPPORTER) return true;
			break;
		case GROUP_SONSTIGE:
			
			if(rpgPlayer.getRank() >= GROUP_SONSTIGE) return true;
			break;
		case GROUP_SPIELER_5:
			
			if(rpgPlayer.getRank() >= GROUP_SPIELER_5) return true;
			break;
		case GROUP_SPIELER_4:
			
			if(rpgPlayer.getRank() >= GROUP_SPIELER_4) return true;
			break;
		case GROUP_SPIELER_3:
			
			if(rpgPlayer.getRank() >= GROUP_SPIELER_3) return true;
			break;
		case GROUP_SPIELER_2:
			
			if(rpgPlayer.getRank() >= GROUP_SPIELER_2) return true;
			break;
		case GROUP_SPIELER_1:
			
			if(rpgPlayer.getRank() >= GROUP_SPIELER_1) return true;
			break;
		}
		
		return false;
	}
	
	public static TextComponent getChatPrefix(Player player){
		
		Tenyrian plugin = Tenyrian.getInstance();
		RpgPlayer rpgPlayer = plugin.getRpgPlayerManager().getRpgPlayer(player);
		
		String rankName = getRankName(rpgPlayer.getRank());
		String rankColor = getRankColor(rpgPlayer.getRank());
		
		TextComponent playername = new TextComponent(rankColor + player.getName());
		playername.setHoverEvent( new HoverEvent( HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(rankColor + rankName).create()));
		playername.setClickEvent( new ClickEvent( ClickEvent.Action.RUN_COMMAND, "/info " + player.getName()));
		return playername;
	}
	
	public static String getRankName(int rank){
		
		switch (rank) {
		case HEAD_ADMIN:
			
			return "Head-Adminstrator";
		case ADMIN:
			
			return "Adminstrator";
		case MODERATOR:
			
			return "Moderator";
		case DEVELOPER:
			
			return "Developer";
		case SUPPORTER:
			
			return "Supporter";
		case PR_MANAGER:
			
			return "PR-Manager";
		case KONZEPTLER:
			
			return "Konzeptler";
		case GRAFIKER:
			
			return "Grafiker";
		case ARCHITEKT:
			
			return "Architekt";
		case PROBEMITGLIED:
			
			return "Anwärter";
		case SPIELER_DIAMANT:
			
			return "Premium-Diamant";
		case SPIELER_GOLD:
			
			return "Premium-Gold";
		case SPIELER_SILBER:
			
			return "Premium-Silber";
		case SPIELER_BRONZE:
			
			return "Premium-Bronze";
		case SPIELER:
			
			return "Spieler";
		default:
			
			return "NULL";
		}
	}
	
	public static String getRankColor(int rank){
		
		switch (rank) {
		case HEAD_ADMIN:
			
			return "§4";
		case ADMIN:
			
			return "§c";
		case MODERATOR:
			
			return "§1";
		case DEVELOPER:
			
			return "§5";
		case SUPPORTER:
			
			return "§b";
		case PR_MANAGER:
			
			return "§e";
		case KONZEPTLER:
			
			return "§e";
		case GRAFIKER:
			
			return "§e";
		case ARCHITEKT:
			
			return "§e";
		case PROBEMITGLIED:
			
			return "§e";
		case SPIELER_DIAMANT:
			
			return "§6";
		case SPIELER_GOLD:
			
			return "§6";
		case SPIELER_SILBER:
			
			return "§6";
		case SPIELER_BRONZE:
			
			return "§6";
		case SPIELER:
			
			return "§7";
		default:
			
			return "§7";
		}
	}
}
