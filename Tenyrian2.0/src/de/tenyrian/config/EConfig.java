package de.tenyrian.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import de.tenyrian.Tenyrian;

public class EConfig {

	private Tenyrian plugin;
	
	private String path;
	private File file;
	
	private YamlConfiguration ymlConfig;
	
	public EConfig(Tenyrian plugin, String path){
		
		this.plugin = plugin;
		this.path = path;

		file = new File(plugin.getDataFolder(), path);
		
		if(!file.exists()){
			
			file.getParentFile().mkdirs();
			
			try {
				
				file.createNewFile();
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
		
		load();
	}
	
	public void load(){
		
		ymlConfig = new YamlConfiguration();
		try {
			
			ymlConfig.load(file);
			
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		} catch (InvalidConfigurationException e) {
			
			e.printStackTrace();
		}
	}
	
	public void save(){
		
		try {
			
			file = new File(plugin.getDataFolder(), path);
			
			if(!file.exists()){
				
				file.getParentFile().mkdirs();
				
				try {
					
					file.createNewFile();
				} catch (IOException e) {
					
					e.printStackTrace();
				}
			}
			
			ymlConfig.save(file);
			
		} catch (IOException e) {

			e.printStackTrace();
		}
	}
	
	public void delete(){
		
		file = new File(plugin.getDataFolder(), path);
		
		if(file.exists()){
			
			file.delete();

		}
	}
	
	public YamlConfiguration getYml(){
		
		return ymlConfig;
	}
}
