package de.tenyrian.config;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;

import de.tenyrian.Tenyrian;
import de.tenyrian.rank.Rank;
import de.tenyrian.utils.LocationUtils;

public class DefaultConfig {

	private FileConfiguration config;
	
	private Tenyrian plugin;
	
	public DefaultConfig(Tenyrian plugin){
		
		this.plugin = plugin;
		
		load();
	}
	
	public void load(){
		
    	config = plugin.getConfig();

    	addDefault("config.database.url", "localhost");
    	addDefault("config.database.port", 3306);
    	addDefault("config.database.user", "root");
    	addDefault("config.database.password", "******");
	    
    	addDefault("config.chat.local.range", 50);
    	
    	addDefault("config.player.level", 1);
    	addDefault("config.player.money", 150);
    	addDefault("config.player.bank", 0);
    	addDefault("config.player.rank", Rank.SPIELER);
    	addDefault("config.player.language", "DE");
    	addDefault("config.spawn.location", Bukkit.getServer().getWorld("world").getSpawnLocation());
    	
	    plugin.saveConfig();
	}
	
	public void addDefault(String path, Object object){
		
		if(!config.isSet(path)) config.set(path, object);
	}
	
	
}
